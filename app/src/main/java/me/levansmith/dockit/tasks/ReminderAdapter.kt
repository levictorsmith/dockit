package me.levansmith.dockit.tasks

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ActivityTaskBinding
import me.levansmith.dockit.domain.model.Reminder

class ReminderAdapter(context: Context, private val activityBinding: ActivityTaskBinding) : RecyclerView.Adapter<ReminderAdapter.ReminderHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val reminders = mutableListOf<String>().apply {
        addAll(activityBinding.task!!.reminders.map { Reminder.getReminderText(it[Reminder.ARG_TIME] as HashMap<String, Any>) }.toMutableList())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ReminderHolder(inflater.inflate(R.layout.list_item_reminder, parent, false))

    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        holder.bind(reminders[position])
    }

    override fun getItemCount(): Int = reminders.size

    fun addItem(time: HashMap<String, Any>) {
        reminders.add(Reminder.getReminderText(time))
        notifyDataSetChanged()
    }

    //TODO: Implement onClickListener so you can change it?
    inner class ReminderHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val reminderText = view.findViewById<TextView>(R.id.reminder)
        private val delete = view.findViewById<ImageButton>(R.id.delete)

        init {
            delete.setOnClickListener {
                activityBinding.task!!.reminders.removeAt(adapterPosition)
                reminders.removeAt(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(text: String) {
            //TODO: Check if that reminder type is already in the list
            reminderText.text = text
        }
    }
}