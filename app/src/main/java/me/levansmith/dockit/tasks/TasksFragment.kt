package me.levansmith.dockit.tasks

import android.annotation.SuppressLint
import androidx.databinding.DataBindingUtil
import android.graphics.Rect
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.DragEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.FragmentTasksBinding
import me.levansmith.dockit.domain.model.Task
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.dockit.domain.model.TaskSerializer
import me.levansmith.dockit.util.*
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logging.logTag
import java.util.*


class TasksFragment : Fragment(), View.OnDragListener {

    private lateinit var binding: FragmentTasksBinding
    private lateinit var planner: DocumentReference
    private lateinit var morningTasksAdapter: TasksAdapter
    private lateinit var afternoonTasksAdapter: TasksAdapter
    private lateinit var eveningTasksAdapter: TasksAdapter
    private lateinit var undoneTasksAdapter: TasksAdapter

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tasks, container, false)
        planner = FirebaseManager.getDocument(arguments!!.getString(ARG_PLANNER)!!)

        //TODO: Change the date here to the selected date
        val morningOptions: FirestoreRecyclerOptions<Task> = FirestoreRecyclerOptions.Builder<Task>()
                .setQuery(TaskClient.getMorningTasksQuery(planner, Date()), TaskSerializer())
                .setLifecycleOwner(this)
                .build()
        val afternoonOptions: FirestoreRecyclerOptions<Task> = FirestoreRecyclerOptions.Builder<Task>()
                .setQuery(TaskClient.getAfternoonTasksQuery(planner, Date()), TaskSerializer())
                .setLifecycleOwner(this)
                .build()
        val eveningOptions: FirestoreRecyclerOptions<Task> = FirestoreRecyclerOptions.Builder<Task>()
                .setQuery(TaskClient.getEveningTasksQuery(planner, Date()), TaskSerializer())
                .setLifecycleOwner(this)
                .build()
        val undoneOptions: FirestoreRecyclerOptions<Task> = FirestoreRecyclerOptions.Builder<Task>()
                .setQuery(TaskClient.getUndoneTasksQuery(planner, Date()), TaskSerializer())
                .setLifecycleOwner(this)
                .build()
        morningTasksAdapter = TasksAdapter(morningOptions, binding.root)
        afternoonTasksAdapter = TasksAdapter(afternoonOptions, binding.root)
        eveningTasksAdapter = TasksAdapter(eveningOptions, binding.root)
        undoneTasksAdapter = TasksAdapter(undoneOptions, binding.root, TasksAdapter.UNDONE)

        binding.morningTasks.adapter = morningTasksAdapter
        binding.afternoonTasks.adapter = afternoonTasksAdapter
        binding.eveningTasks.adapter = eveningTasksAdapter
        binding.undoneTasks.adapter = undoneTasksAdapter

        binding.root.setOnDragListener(this)

        binding.morningTasksHeader.setOnTouchListener(OnDrawableClickListener(OnDrawableClickListener.Position.RIGHT) {
            TaskActivity.startAdd(context, calMorning())
            return@OnDrawableClickListener true
        })
        binding.afternoonTasksHeader.setOnTouchListener(OnDrawableClickListener(OnDrawableClickListener.Position.RIGHT) {
            TaskActivity.startAdd(context, calAfternoon())
            return@OnDrawableClickListener true
        })
        binding.eveningTasksHeader.setOnTouchListener(OnDrawableClickListener(OnDrawableClickListener.Position.RIGHT) {
            TaskActivity.startAdd(context, calEvening())
            return@OnDrawableClickListener true
        })

        ItemTouchHelper(TouchHelperCallback(morningTasksAdapter)).attachToRecyclerView(binding.morningTasks)
        ItemTouchHelper(TouchHelperCallback(afternoonTasksAdapter)).attachToRecyclerView(binding.afternoonTasks)
        ItemTouchHelper(TouchHelperCallback(eveningTasksAdapter)).attachToRecyclerView(binding.eveningTasks)
        ItemTouchHelper(TouchHelperCallback(undoneTasksAdapter)).attachToRecyclerView(binding.undoneTasks)

        binding.morningTasks.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        binding.afternoonTasks.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        binding.eveningTasks.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        binding.undoneTasks.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))

        return binding.root
    }

    private fun isWithinRect(rect: Rect, y: Float): Boolean = rect.top <= y && y <= rect.bottom

    override fun onDrag(v: View, event: DragEvent): Boolean {
        val action = event.action
        if (action == DragEvent.ACTION_DROP) {
            val morning = binding.morningTasksContainer.getRect()
            val afternoon = binding.afternoonTasksContainer.getRect()
            val evening = binding.eveningTasksContainer.getRect()
            val dropData = event.clipData.getItemAt(0).text.toString()
            val task = Task.deserialize(dropData)
            task.startTime = when {
                isWithinRect(morning, event.y) -> dateMorning()
                isWithinRect(afternoon, event.y) -> dateAfternoon()
                isWithinRect(evening, event.y) -> dateEvening()
                else -> task.startTime
            }
            TaskClient.addTask(task)
        }
        return true
    }

    companion object {

        const val ARG_PLANNER = "me.levansmith.dockit.tasks.TasksFragment.ARG_PLANNER"
        val TAG = TasksFragment::class.java.logTag()

        fun newInstance(plannerPath: String): TasksFragment {
            val fragment = TasksFragment()
            val args = Bundle()
            args.putString(ARG_PLANNER, plannerPath)
            fragment.arguments = args
            return fragment
        }
    }
}