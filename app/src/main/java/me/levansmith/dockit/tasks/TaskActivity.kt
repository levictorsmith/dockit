package me.levansmith.dockit.tasks

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_task.*
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ActivityTaskBinding
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.domain.model.Task
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.dockit.util.*
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class TaskActivity : AppCompatActivity(), AppBarStateChangeListener, TimePicker.OnTimeChangedListener, DatePicker.OnDateChangedListener, View.OnClickListener {

    companion object {

        const val ARG_PLANNER = "me.levansmith.dockit.tasks.AddTaskActivity.ARG_PLANNER"
        const val ARG_CALENDAR = "me.levansmith.dockit.tasks.AddTaskActivity.ARG_CALENDAR"
        const val START_TIME = "me.levansmith.dockit.tasks.AddTaskActivity.START_TIME"
        const val END_TIME = "me.levansmith.dockit.tasks.AddTaskActivity.END_TIME"
        const val START_DATE = "me.levansmith.dockit.tasks.AddTaskActivity.START_DATE"
        const val END_DATE = "me.levansmith.dockit.tasks.AddTaskActivity.END_DATE"
        const val EDIT = "me.levansmith.dockit.tasks.TaskActivity.EDIT"

        fun startAdd(context: Context) {
            val intent = Intent(context, TaskActivity::class.java)
            context.startActivity(intent)
        }

        fun startAdd(context: Context?, cal: Calendar) {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(ARG_CALENDAR, cal)
            context?.startActivity(intent)
        }

        fun startEdit(context: Context, task: Task) {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(EDIT, Task.serialize(task))
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityTaskBinding
    private var isEdit: Boolean = false
    private var showDescription: Boolean = true
//    private lateinit var reminderAdapter: ReminderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_task)
        isEdit = intent.hasExtra(EDIT)
        setSupportActionBar(toolbar)
        binding.task = if (isEdit) Task.deserialize(intent.getStringExtra(EDIT)) else Task()
        if (intent.hasExtra(ARG_CALENDAR)) {
            val startTime: Calendar = intent.getSerializableExtra(ARG_CALENDAR) as Calendar
            binding.task!!.startTime = startTime.time
        }
        PlannerClient.getMainPlanner { planner ->
            binding.task!!.plannerId = planner
        }
        supportActionBar?.setTitle(if (isEdit) R.string.title_activity_edit_task else R.string.title_activity_add_task)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        app_bar.addOnOffsetChangedListener(this)
        fab.setImageResource(if (isEdit) R.drawable.ic_done_white_24dp else R.drawable.ic_add_white_24dp)
        fab.setOnClickListener(this)
//        reminderAdapter = ReminderAdapter(this, binding)
//        binding.reminders.adapter = reminderAdapter
//        binding.reminders.layoutManager = LinearLayoutManager(this)
        binding.actionAddSave.setText(if (isEdit) R.string.action_save_changes else R.string.title_activity_add_task)

        task_description_top.addTextChangedListener {
            if (task_description_top.isFocused) {
                task_description_bottom.text = it
            }
        }
        task_description_bottom.addTextChangedListener {
            if (task_description_bottom.isFocused) {
                task_description_top.text = it
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.task_start_date -> {
                val cal = DateUtils.toCalendar(binding.task?.startTime)
                DatePicker.newInstance(cal, this, START_DATE)
            }
            R.id.task_start_time -> {
                val cal = DateUtils.toCalendar(binding.task?.startTime)
                TimePicker.newInstance(cal, this, START_TIME)
            }
            R.id.reminders_title -> {
//                showRemindersDialog()
            }
            R.id.action_add_save -> {
                if (isEdit) {
                    editTask()
                } else {
                    addTask()
                }
            }
            R.id.fab -> {
                if (isEdit) {
                    editTask()
                } else {
                    addTask()
                }
            }
        }
    }

    private fun editTask() {
        TaskClient.updateTask(binding.task!!)
        //TODO: Add/remove/update reminders to cache on update
        finish()
    }

    private fun addTask() {
        TaskClient.addTask(binding.task!!) {
            //            launch {
//                binding.task!!.reminders.forEach {
//                    DockitApp.cache.reminderDao().insert(ReminderEntity(0, Reminder.getReminderTime(binding.task!!.startTime, it[Reminder.ARG_TIME] as HashMap<String, Any>).time, Constants.Types.TASK, task.id))
//                }
//            }
        }
        finish()
    }

    override fun onDateChanged(calendar: Calendar, tag: String) {
        when (tag) {
            START_DATE -> {
                binding.task!!.startTime = calendar.time
                binding.invalidateAll()
            }
            END_DATE -> {
//                binding.taskEndDate.text = DateFormat.getMediumDateFormat(this).format(calendar.time)
            }
        }
    }

    override fun onTimeChanged(calendar: Calendar, tag: String, data: Bundle) {
        when (tag) {
            START_TIME -> {
                binding.task!!.startTime = calendar.time
                binding.invalidateAll()
            }
            END_TIME -> {
//                binding.taskEndTime.text = DateFormat.getTimeFormat(this).format(calendar.time)
            }
        }
    }

    override fun onStateChanged(appBarLayout: AppBarLayout, state: AppBarStateChangeListener.State, i: Int) {
        if (i >= 160 && showDescription) {
            task_description_container_top.hide()
            task_description_container_bottom.show()
            showDescription = !showDescription
        } else if (i < 150 && !showDescription) {
            task_description_container_top.show()
            task_description_container_bottom.hide()
            showDescription = !showDescription
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

//    private fun showRemindersDialog() {
//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("Schedule a reminder")
//                .setItems(R.array.reminder_types, { _, which ->
//                    val choice = resources.obtainTypedArray(R.array.reminder_types).getResourceId(which, 0)
//                    val reminder = mutableMapOf<String, Any>().apply {
//                        put(Reminder.ARG_TIME, when (choice) {
//                            R.string.reminder_10_min -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 10), Pair(Reminder.ARG_TIME_UNIT, Reminder.MINUTES))
//                            R.string.reminder_30_min -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 30), Pair(Reminder.ARG_TIME_UNIT, Reminder.MINUTES))
//                            R.string.reminder_1_hour -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 1), Pair(Reminder.ARG_TIME_UNIT, Reminder.HOURS))
//                            R.string.reminder_2_hour -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 2), Pair(Reminder.ARG_TIME_UNIT, Reminder.HOURS))
//                            R.string.reminder_1_day -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 1), Pair(Reminder.ARG_TIME_UNIT, Reminder.DAYS))
//                            else -> hashMapOf(Pair(Reminder.ARG_TIME_NUM, 10), Pair(Reminder.ARG_TIME_UNIT, Reminder.MINUTES)) //Default to 10 minutes before
//                        })
//                        put(Reminder.ARG_TYPE, Constants.Types.TASK)
//                        put(Reminder.ARG_ITEM_REF, binding.task!!.ref)
//                    }
//
//                    //TODO: Add to task reminder list
//                    if (!binding.task!!.reminders.contains(reminder)) {
//                        binding.task!!.reminders.add(reminder)
//                        reminderAdapter.addItem(reminder[Reminder.ARG_TIME] as HashMap<String, Any>)
//                    }
//                })
//        builder.create()
//                .show()
//    }

}
