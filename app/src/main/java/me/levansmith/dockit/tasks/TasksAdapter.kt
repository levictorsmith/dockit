package me.levansmith.dockit.tasks

import android.content.ClipData
import android.os.Build
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.LinearLayout
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ListItemTaskBinding
import me.levansmith.dockit.domain.model.Task
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.dockit.util.*
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils


class TasksAdapter(
        options: FirestoreRecyclerOptions<Task>,
        private val rootView: View,
        private val type: String = DAY_TASKS
) : FirestoreRecyclerAdapter<Task, TasksAdapter.TaskHolder>(options), ItemTouchHelperListener {

    companion object {
        val TAG = TasksAdapter::class.java.logTag()
        const val DAY_TASKS = "me.levansmith.dockit.tasks.TasksAdapter.DAY_TASKS"
        const val UNDONE = "me.levansmith.dockit.tasks.TasksAdapter.UNDONE"
        const val TIME_PICKER = "me.levansmith.dockit.tasks.TasksAdapter.TIME_PICKER"
    }

    private var tempDeletedItem: Task? = null

    override fun onItemDismiss(position: Int) {
        tempDeletedItem = getItem(position)
        TaskClient.deleteTask(snapshots.getSnapshot(position).id)
        Snackbar.make(rootView, "Deleted task", Snackbar.LENGTH_LONG)
                .setAction("Undo") {
                    if (tempDeletedItem != null) {
                        TaskClient.addTask(tempDeletedItem!!)
                    }
                }
                .setActionTextColor(ContextCompat.getColor(rootView.context, R.color.colorAccent))
                .show()
    }

    override fun onItemMove(viewHolder: RecyclerView.ViewHolder, fromPosition: Int, toPosition: Int) {
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onItemOutOfBounds(viewHolder: RecyclerView.ViewHolder, state: ItemState) {
        if ((viewHolder as TaskHolder).state is ItemState.Idle) {
            viewHolder.state = state
            val shadowBuilder = View.DragShadowBuilder(viewHolder.itemView)
            val task = viewHolder.binding.task!!
            val dropData = ClipData.newPlainText(Constants.TASK_DRAG_DROP_DATA, Task.serialize(task))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                viewHolder.itemView.startDragAndDrop(dropData, shadowBuilder, viewHolder.itemView, 0)
            } else {
                viewHolder.itemView.startDrag(dropData, shadowBuilder, viewHolder.itemView, 0)
            }
            //TODO: Maybe not delete it and re-create it but rather mark it as "not done" then just change the date
            TaskClient.deleteTask(task.ref)
            notifyItemRangeChanged(0, itemCount)
        }
    }


    override fun onDataChanged() {
        super.onDataChanged()
        if (snapshots.isEmpty()) {
            if(type == UNDONE) {
                rootView.findViewById<LinearLayout>(R.id.undone_tasks_container).hide()
            }
        } else {
            if (type == UNDONE) {
                rootView.findViewById<LinearLayout>(R.id.undone_tasks_container).show()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        return TaskHolder(ListItemTaskBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: TaskHolder, position: Int, model: Task) {
        holder.bind(getItem(position))
    }

    inner class TaskHolder(val binding: ListItemTaskBinding) : RecyclerView.ViewHolder(binding.root),
            CompoundButton.OnCheckedChangeListener, View.OnClickListener {

        var state: ItemState = ItemState.Idle

        init {
            binding.completed.setOnCheckedChangeListener(this)
            binding.root.setOnClickListener(this)
            binding.time.setOnClickListener(this)
        }

        fun bind(task: Task) {
            binding.task = task
            state = ItemState.Idle
        }

        override fun onClick(view: View) {
            if (view.id == R.id.time) {
                val params = Bundle()
                params.putString(TaskClient.Fields.REF, binding.task?.ref)
                TimePicker.newInstance(DateUtils.toCalendar(binding.task?.startTime), (rootView.context as AppCompatActivity), TIME_PICKER, params)
            } else {
                TaskActivity.startEdit(binding.root.context, binding.task!!)
            }
        }

        override fun onCheckedChanged(checkBox: CompoundButton?, isChecked: Boolean) {
            TaskClient.updateTask(snapshots.getSnapshot(adapterPosition).id, mutableMapOf<String, Any>().apply {
                put(TaskClient.Fields.COMPLETED, isChecked)
            })
        }
    }
}