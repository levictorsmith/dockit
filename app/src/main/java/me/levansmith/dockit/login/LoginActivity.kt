package me.levansmith.dockit.login

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import me.levansmith.dockit.R
import me.levansmith.dockit.dashboard.DashboardActivity
import me.levansmith.dockit.databinding.ActivityLoginBinding
import me.levansmith.dockit.domain.model.Planner
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.util.Constants
import me.levansmith.dockit.util.manager.AuthManager

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(), AuthManager.AuthListener, View.OnClickListener {

    companion object {

        private val LOG_TAG = LoginActivity::class.toString()

        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //TODO: Use data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.googleSignInButton.setOnClickListener(this)
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }

    private fun toggleSpinner(showSpinner: Boolean) {
        binding.spinner.isEnabled = showSpinner
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RequestCodes.SIGN_IN_GOOGLE) {
            val result: GoogleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            // Google sign in was success, now authenticate with Firebase
            if (result.isSuccess) {
                val account: GoogleSignInAccount = result.signInAccount!!
                AuthManager.authenticateGoogleUser(this, account)
            } else {
                // Google sign in failed
                //TODO: Update UI appropriately
                Log.e("TAG", "Google Sign in failed: " + result.status.toString())
            }
        }
    }

    override fun onComplete() {
        Log.d(LOG_TAG, "Success!" + AuthManager.user?.email)
        toggleSpinner(false)
        //TODO: Maybe create a selection activity to choose which planner?
        PlannerClient.getMainPlanner {
            if (it == null) {
                PlannerClient.addPlanner(Planner("Personal", AuthManager.uid)) {
                    DashboardActivity.start(this)
                    finish()
                }
            } else {
                DashboardActivity.start(this)
                finish()
            }
        }
    }

    override fun onError(e: Exception) {
        toggleSpinner(false)
        when (e) {
            is FirebaseAuthInvalidCredentialsException -> {
                Log.e(LOG_TAG, "ERROR: Wrong username or password")
            }
        }
    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.email_sign_in_button) {
            val email = binding.email.text.toString()
            val password = binding.password.text.toString()
            AuthManager.subscribe(this).signInEmailUser(this, email, password)
            toggleSpinner(true)
        } else if (id == R.id.google_sign_in_button) {
            AuthManager.subscribe(this).signInGoogleUser(this)
            toggleSpinner(true)
        }
    }
}

