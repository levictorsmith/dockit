package me.levansmith.dockit.launcher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import me.levansmith.dockit.dashboard.DashboardActivity
import me.levansmith.dockit.login.LoginActivity
import me.levansmith.dockit.util.manager.AuthManager

class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (AuthManager.isLoggedIn) {
            DashboardActivity.start(this)
        } else {
            LoginActivity.start(this)
        }
        finish()
    }
}
