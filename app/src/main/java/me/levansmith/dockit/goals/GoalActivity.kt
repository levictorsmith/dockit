package me.levansmith.dockit.goals

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_goal.*
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ActivityGoalBinding
import me.levansmith.dockit.domain.model.Goal
import me.levansmith.dockit.domain.model.GoalClient
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.util.*
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class GoalActivity : AppCompatActivity(), View.OnClickListener, DatePicker.OnDateChangedListener, AppBarStateChangeListener {

    companion object {

        const val EDIT = "me.levansmith.dockit.goals.GoalActivity.EDIT"

        fun startAdd(context: Context?) {
            val intent = Intent(context, GoalActivity::class.java)
            context?.startActivity(intent)
        }
        fun startEdit(context: Context, goal: Goal) {
            val intent = Intent(context, GoalActivity::class.java)
            intent.putExtra(EDIT, goal)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityGoalBinding
    private var isEdit: Boolean = false
    private var showDescription: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goal)
        initUI()
    }

    private fun initUI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_goal)
        isEdit = intent.hasExtra(EDIT)
        setSupportActionBar(toolbar)
        binding.goal = if (isEdit) intent.getParcelableExtra(EDIT) else Goal()
        PlannerClient.getMainPlanner { planner ->
            binding.goal!!.plannerId = planner
        }
        supportActionBar?.setTitle(if (isEdit) R.string.title_activity_edit_goal else R.string.title_activity_add_goal)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        app_bar.addOnOffsetChangedListener(this)
        fab.setImageResource(if (isEdit) R.drawable.ic_done_white_24dp else R.drawable.ic_add_white_24dp)
        fab.setOnClickListener(this)
        binding.goalType.adapter = GoalTypeAdapter(this, intArrayOf(R.drawable.ic_view_day_black_24dp, R.drawable.ic_view_week_black_24dp, R.drawable.ic_calendar_blank), R.array.goal_types)
        binding.actionAddSave.setText(if (isEdit) R.string.action_save_changes else R.string.title_activity_add_goal)

        goal_description_top.addTextChangedListener {
            if (goal_description_top.isFocused) {
                goal_description_bottom.text = it
            }
        }
        goal_description_bottom.addTextChangedListener {
            if (goal_description_bottom.isFocused) {
                goal_description_top.text = it
            }
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.goal_start_date -> {
                val cal = DateUtils.toCalendar(binding.goal?.startDate)
                DatePicker.newInstance(cal, this)
            }
            R.id.action_add_save -> {
                if (isEdit) {
                    editGoal()
                } else {
                    addGoal()
                }
            }
            R.id.fab -> {
                if (isEdit) {
                    editGoal()
                } else {
                    addGoal()
                }
            }
        }
    }

    fun addGoal() {
        GoalClient.addGoal(binding.goal!!)
        finish()
    }

    fun editGoal() {
        GoalClient.updateGoal(binding.goal!!)
        finish()
    }

    override fun onDateChanged(calendar: Calendar, tag: String) {
        binding.goal!!.startDate = calendar.time
        binding.invalidateAll()
    }

    override fun onStateChanged(appBarLayout: AppBarLayout, state: AppBarStateChangeListener.State, i: Int) {
        if (i >= 160 && showDescription) {
            goal_description_container_top.hide()
            goal_description_container_bottom.show()
            showDescription = !showDescription
        } else if (i < 150 && !showDescription) {
            goal_description_container_top.show()
            goal_description_container_bottom.hide()
            showDescription = !showDescription
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
