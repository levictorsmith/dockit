package me.levansmith.dockit.goals

import android.content.Context
import androidx.annotation.ArrayRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.list_item_goal_type.view.*
import me.levansmith.dockit.R

class GoalTypeAdapter(private val context: Context, private val icons: IntArray, @ArrayRes private val goalTypeArray: Int) : BaseAdapter() {

    private val types = context.resources.getStringArray(goalTypeArray)

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {
        val retView = LayoutInflater.from(context).inflate(R.layout.list_item_goal_type, null)
        retView.goal_type_icon.setImageResource(icons[i])
        retView.goal_type.text = types[i]
        return retView
    }

    override fun getItem(p0: Int): Any? = null
    override fun getItemId(p0: Int): Long = 0
    override fun getCount(): Int = icons.size
}
