package me.levansmith.dockit.goals

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import me.levansmith.dockit.databinding.ListItemGoalBinding
import me.levansmith.dockit.domain.model.Goal


class GoalsAdapter(options: FirestoreRecyclerOptions<Goal>) : FirestoreRecyclerAdapter<Goal, GoalsAdapter.GoalHolder>(options) {

    override fun onBindViewHolder(holder: GoalHolder, position: Int, model: Goal) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalHolder {
        return GoalHolder(ListItemGoalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    inner class GoalHolder(private val binding: ListItemGoalBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            GoalActivity.startEdit(binding.root.context, binding.goal!!)
        }

        fun bind(goal: Goal) {
            binding.goal = goal
            binding.goal?.ref = snapshots.getSnapshot(adapterPosition).id
        }
    }
}

