package me.levansmith.dockit

import android.app.Application
import androidx.room.Room
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import me.levansmith.dockit.domain.AppDatabase


class DockitApp : Application() {
//    lateinit var injector: Injector
//        private set
    lateinit var db: FirebaseFirestore
        private set

    override fun onCreate() {
        super.onCreate()
        self = this
//        injector = DaggerInjector.create()
        db = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()
        db.firestoreSettings = settings
        cache = Room.databaseBuilder(self, AppDatabase::class.java, DATABASE_NAME).build()
    }

    companion object {
        lateinit var self: DockitApp
        lateinit var cache: AppDatabase
        const val DATABASE_NAME = "dockit"
    }
}
