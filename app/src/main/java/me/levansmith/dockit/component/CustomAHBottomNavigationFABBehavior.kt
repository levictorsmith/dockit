package me.levansmith.dockit.component

import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import android.view.View
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlin.math.roundToInt

class CustomAHBottomNavigationFABBehavior : CoordinatorLayout.Behavior<View>() {

    override fun layoutDependsOn(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        return (dependency is Snackbar.SnackbarLayout || dependency is AHBottomNavigation)
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        return when (dependency) {
            is Snackbar.SnackbarLayout -> {
//            lastSnackbarUpdate = System.currentTimeMillis()
                val bottomMargin = (child.layoutParams as CoordinatorLayout.LayoutParams).bottomMargin
                child.y = (dependency.y.roundToInt() - (child.height + bottomMargin)).toFloat()
                true
            }
            is AHBottomNavigation -> {
//            // Hack to avoid moving the FAB when the AHBottomNavigation is moving (showing or hiding animation)
//            if (System.currentTimeMillis() - lastSnackbarUpdate < 30) {
//                return
//            }
                val bottomMargin = (child.layoutParams as CoordinatorLayout.LayoutParams).bottomMargin
                child.y = (dependency.y.roundToInt() - (child.height + bottomMargin)).toFloat()
                true
            }
            else -> false
        }
    }

    override fun onDependentViewRemoved(parent: CoordinatorLayout, child: View, dependency: View) {
        if (dependency is Snackbar.SnackbarLayout) {
//            lastSnackbarUpdate = System.currentTimeMillis()
            val bottomMargin = (child.layoutParams as CoordinatorLayout.LayoutParams).bottomMargin
            child.y = (dependency.y.roundToInt() - (child.height + bottomMargin)).toFloat()
        } else if (dependency is AHBottomNavigation) {
//            // Hack to avoid moving the FAB when the AHBottomNavigation is moving (showing or hiding animation)
//            if (System.currentTimeMillis() - lastSnackbarUpdate < 30) {
//                return
//            }
            val bottomMargin = (child.layoutParams as CoordinatorLayout.LayoutParams).bottomMargin
            child.y = (dependency.y.roundToInt() - (child.height + bottomMargin)).toFloat()
        }
    }

}
