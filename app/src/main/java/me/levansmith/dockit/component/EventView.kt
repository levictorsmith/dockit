package me.levansmith.dockit.component

import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import me.levansmith.dockit.R
import me.levansmith.dockit.domain.model.Event
import me.levansmith.dockit.schedule.EventActivity

class EventView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : TextView(context, attrs, defStyleAttr), View.OnClickListener {

    lateinit var event: Event

    constructor(context: Context, event: Event) : this(context) {
        this.event = event
        id = View.generateViewId()
        text = event.title
        //Account for margin on all sides
        val margin = context.resources.getDimension(R.dimen.event_item_margin).toInt()
        val leftMargin = context.resources.getDimension(R.dimen.material_24dp).toInt()
        //Width is determined by the HourView (which the EventView will span) with the most events
        //Height is determined by the number of HourViews spanned by the EventView
        val params = ConstraintLayout.LayoutParams(0,0)
        params.setMargins(leftMargin, 0, margin, margin)
        params.matchConstraintDefaultHeight = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT
        params.matchConstraintDefaultWidth = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT
        layoutParams = params
        background = context.getDrawable(R.drawable.event_background)
        val attrArray: TypedArray = context.obtainStyledAttributes(intArrayOf(R.attr.selectableItemBackground))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            foreground = attrArray.getDrawable(0)
        }
        attrArray.recycle()

        setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        EventActivity.startEdit(context, this.event)
    }
}