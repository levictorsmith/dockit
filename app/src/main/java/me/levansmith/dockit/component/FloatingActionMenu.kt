package me.levansmith.dockit.component

import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.core.content.ContextCompat
import android.transition.TransitionManager
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.TextView
import me.levansmith.dockit.DockitApp
import me.levansmith.dockit.R
import me.levansmith.dockit.util.applyConstraints
import me.levansmith.dockit.util.forEachChild
import me.levansmith.dockit.util.hide
import me.levansmith.dockit.util.show

class FloatingActionMenu @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    companion object {
        private const val FAB_SIZE_NORMAL = 0
        private const val FAB_SIZE_MINI = 1
        const val DEFAULT_ROOT_SIZE = FAB_SIZE_NORMAL
        val DEFAULT_ROOT_ICON: Drawable = ContextCompat.getDrawable(DockitApp.self, R.drawable.ic_add_white_24dp)!!
    }

    private val labels: MutableMap<Int, TextView> = mutableMapOf()

    lateinit var rootFAB: FloatingActionButton
    private var rootFABIcon: Drawable
    private var rootFABSize: Int
    private val rootId: Int = R.id.fam_root_fab
    var isOpen: Boolean = false

    init {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.FloatingActionMenu, 0, 0)
        rootFABIcon = a.getDrawable(R.styleable.FloatingActionMenu_fam_rootIcon) ?: DEFAULT_ROOT_ICON
        rootFABSize = a.getInteger(R.styleable.FloatingActionMenu_fam_rootSize, DEFAULT_ROOT_SIZE)
        a.recycle()
        addRootFAB()
    }

    private fun addRootFAB() {
        val rootFab = FloatingActionButton(context)
        rootFab.id = rootId
        rootFab.setImageDrawable(rootFABIcon)
        rootFab.size = when (rootFABSize) {
            FAB_SIZE_MINI -> FloatingActionButton.SIZE_MINI
            else -> FloatingActionButton.SIZE_NORMAL
        }
        rootFab.setOnClickListener {
            toggle()
        }
        addView(rootFab)
        val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
        (rootFab.layoutParams as LayoutParams).setMargins(margin, margin, margin, margin)

        applyConstraints {
            it.connect(rootFab.id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
            it.connect(rootFab.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
        }
        rootFAB = rootFab
    }

    override fun onViewAdded(view: View?) {
        super.onViewAdded(view)
        if (view != null && view.id != rootId && view !is TextView) {
            view.hide()
            if (view.id == View.NO_ID) {
                view.id = View.generateViewId()
            }
            val label = TextView(context, null, R.attr.fam_labelStyle)
            label.id = View.generateViewId()
            label.text = (view as? ImageButton)?.contentDescription
            labels[view.id] = label
            label.hide()
            addView(label)
            applyConstraints {
                it.centerHorizontally(view.id, rootId)
                it.connect(view.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
            }
        }
    }


    fun toggle() {
        if (isOpen) close() else open()
    }

    fun open() {
        TransitionManager.beginDelayedTransition(this)
        val ids = mutableListOf<@IdRes Int>()
        //Show the child views and keep track of their ids
        forEachChild {
            if (it.id != rootId && it !is TextView) {
                it.show()
                ids.add(it.id)
            }
        }
        //Chain the child views
        ids.forEachIndexed { index, id ->
            if (id != ids.last()) {
                applyConstraints {
                    it.connect(id, ConstraintSet.BOTTOM, ids[index + 1], ConstraintSet.TOP)
                }
            }
        }
        //Connect last child view to root fab
        if (ids.isNotEmpty()) {
            applyConstraints {
                it.connect(ids.last(), ConstraintSet.BOTTOM, rootId, ConstraintSet.TOP)
            }
        }
        //Take care of showing the labels without screwing everything else up
        labels.forEach {
            it.value.show()
            val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
            (it.value.layoutParams as LayoutParams).marginEnd = margin
            applyConstraints { set ->
                set.connect(it.value.id, ConstraintSet.END, it.key, ConstraintSet.START)
                set.centerVertically(it.value.id, it.key)
            }
        }
        val rotate = AnimationUtils.loadAnimation(context, R.anim.rotate_overshoot_open)
        rootFAB.startAnimation(rotate)
        isOpen = true
    }

    fun close() {
        TransitionManager.beginDelayedTransition(this)
        labels.forEach {
                        it.value.hide()
            applyConstraints { set ->
                set.centerVertically(it.value.id, rootId)
            }
        }
        forEachChild {
            if (it.id != rootId && !labels.containsValue(it)) {
                applyConstraints { set ->
                    set.connect(it.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                }
                it.hide()
            }
        }
        val rotate = AnimationUtils.loadAnimation(context, R.anim.rotate_overshoot_close)
        rootFAB.startAnimation(rotate)
        isOpen = false
    }
}