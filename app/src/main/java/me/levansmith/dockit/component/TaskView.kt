package me.levansmith.dockit.component

import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.pchmn.materialchips.ChipView
import me.levansmith.dockit.domain.model.Task
import me.levansmith.dockit.tasks.TaskActivity


class TaskView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ChipView(context, attrs) {

    constructor(context: Context, task: Task): this(context) {
        id = View.generateViewId()
        label = task.description
        val params = ConstraintLayout.LayoutParams(0,0)
        params.height = ConstraintLayout.LayoutParams.WRAP_CONTENT
        params.width = ConstraintLayout.LayoutParams.WRAP_CONTENT
        layoutParams = params
        setOnChipClicked {
            TaskActivity.startEdit(context, task)
        }
    }
}