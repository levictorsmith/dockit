package me.levansmith.dockit.component

import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import me.levansmith.dockit.R
import me.levansmith.dockit.schedule.EventActivity
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class HourView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr), View.OnClickListener {

    private var is24Hour: Boolean = false
    var time: Calendar = DateUtils.truncate(Calendar.getInstance(), Calendar.MINUTE)

    constructor(context: Context, dateTime: Calendar, is24Hour: Boolean = false) : this(context) {
        this.time = DateUtils.truncate(dateTime, Calendar.MINUTE)
        this.is24Hour = is24Hour
        init()
    }

    private fun init() {
        inflate(context, R.layout.list_item_schedule, this)
        id = View.generateViewId()
        var hour = if(is24Hour) time[Calendar.HOUR_OF_DAY] else time[Calendar.HOUR]
        if (hour == 0 && !is24Hour) {
            hour = 12
        }
        val min = time[Calendar.MINUTE]
        if (min != 30) {
            val hourView = findViewById<TextView>(R.id.hour)
            hourView.text = hour.toString()
        } else {
            setBackgroundColor(ContextCompat.getColor(context, R.color.gray_10))
        }
        setOnClickListener(this)
        tag = time[Calendar.HOUR_OF_DAY].toString() + ":" + min.toString()
    }

    override fun onClick(view: View?) {
        EventActivity.startAdd(context, DateUtils.toCalendar(time.time))
    }
}
