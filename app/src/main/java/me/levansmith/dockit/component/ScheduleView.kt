package me.levansmith.dockit.component

import android.content.Context
import android.content.res.TypedArray
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import com.google.firebase.firestore.Query
import me.levansmith.dockit.R
import me.levansmith.dockit.domain.model.Event
import me.levansmith.dockit.domain.model.Task
import me.levansmith.dockit.util.applyConstraints
import me.levansmith.dockit.util.isWithin__After
import me.levansmith.dockit.util.toEnd
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class ScheduleView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    companion object {
        val TAG = ScheduleView::class.java.logTag()
        const val DEFAULT_START_HOUR = 6
        const val DEFAULT_END_HOUR = 23
        const val MORNING_DIVIDER = 11
        const val AFTERNOON_DIVIDER = 17
    }

    private var startHour: Int
    private var endHour: Int
    private var is24Hour: Boolean
    var date: Calendar = Calendar.getInstance()
        set(date) {
            createViews(date)
        }

    var adapter: Adapter = Adapter()

    init {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ScheduleView, 0, 0)
        startHour = a.getInt(R.styleable.ScheduleView_startHour, DEFAULT_START_HOUR)
        endHour = a.getInt(R.styleable.ScheduleView_endHour, DEFAULT_END_HOUR)
        is24Hour = a.getBoolean(R.styleable.ScheduleView_is24Hour, false)
        a.recycle()
    }

    private fun createViews(date: Calendar) {
        removeAllViews()
        if (startHour >= endHour) throw IllegalArgumentException("Start hour must begin before End hour.")
        //All calculation is assuming 24 hour. Not until display will it become 12 hour AM/PM
        val views = mutableListOf<View>()
        //Add hour-views
        for (i in startHour..endHour) {
            createHourViews(i, views, date)
            //Add morning/afternoon/evening dividers
            if (i == MORNING_DIVIDER || i == AFTERNOON_DIVIDER) {
                val divider = View(context)
                divider.id = View.generateViewId()
                divider.layoutParams = LayoutParams(0, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2f, resources.displayMetrics).toInt())
                divider.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
                views.add(divider)
                addView(divider)
            }
        }
        //Add all view constraints
        applyConstraints { set ->
            set.createVerticalChain(views.first().id, ConstraintSet.TOP, views.last().id, ConstraintSet.BOTTOM, views.map { it.id }.toIntArray(), null, ConstraintSet.CHAIN_PACKED)
            for (i in views.indices) {
                when (i) {
                    0 -> set.connect(views[i].id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
                    views.size - 1 -> set.connect(views[i].id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                }
                set.connect(views[i].id, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START)
                set.connect(views[i].id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
                set.constrainWidth(views[i].id, ConstraintSet.MATCH_CONSTRAINT)
            }
        }
    }

    private fun createHourViews(hour: Int, views: MutableList<View>, date: Calendar) {
        //Add starting hour view
        val start = DateUtils.toCalendar(date.time)
        start[Calendar.HOUR_OF_DAY] = hour
        start[Calendar.MINUTE] = 0
        val startView = HourView(context, start, is24Hour)
        views.add(startView)
        addView(startView)

        //Add ending hour view
        val end = DateUtils.toCalendar(date.time)
        end[Calendar.HOUR_OF_DAY] = hour
        end[Calendar.MINUTE] = 30
        val endView = HourView(context, end, is24Hour)
        views.add(endView)
        addView(endView)
    }

    private fun drawEvents(events: List<Event>) {
        //All event views will appear after the HourViews. There should be a more thorough way to ensure this, but I'm lazy
        removeViews(38, childCount - 38)

        val collections = mutableListOf<EventCollection>()
        val hourViews = (0 until childCount).mapNotNull { getChildAt(it) as? HourView }
        //Create the EventViews
        for (event in events) {
            val beginHour = hourViews.find {
                event.startTime.isWithin__After(30, Calendar.MINUTE, it.time, false)
            }
            val endHour = hourViews.findLast {
                event.endTime.isWithin__After(30, Calendar.MINUTE, it.time, true)
            }
            val eventView = EventView(context, event)
            this.addView(eventView)

            applyConstraints {
                it.connect(eventView.id, ConstraintSet.TOP, beginHour?.id ?: hourViews.first().id, ConstraintSet.TOP)
                it.connect(eventView.id, ConstraintSet.BOTTOM, endHour?.id ?: hourViews.last().id, ConstraintSet.BOTTOM)
                it.centerHorizontally(eventView.id, ConstraintSet.PARENT_ID)
            }

            collections.find { event.startTime < it.endTime }?.apply {
                this.events.add(eventView)
                this.endTime = maxOf(event.endTime, this.endTime)
            }
            val children = mutableListOf<EventView>()
            children.add(eventView)
            collections.add(EventCollection(event.startTime, event.endTime, children))
        }

        //TODO: Find parents and children
        //TODO: Find 'split-groups' in collections
        //TODO: Sort 'split-groups' events
        setEventWidthConstraints(collections)
    }

    private fun drawTasks(tasks: List<Task>) {
        (0 until childCount).mapNotNull { getChildAt(it) as? HourView }.forEach { hourView ->
            val hourTasks = tasks.filter { it.startTime.isWithin__After(30, Calendar.MINUTE, hourView.time, false) }.map {
                val taskChip = TaskView(context, it)
                this.addView(taskChip)
                return@map taskChip
            }
            applyConstraints {
                if (hourTasks.size > 1) {
                    it.toEnd(hourTasks, ConstraintSet.PARENT_ID)
                } else if (hourTasks.isNotEmpty()) {
                    it.connect(hourTasks[0].id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
                }
                hourTasks.forEach { taskView ->
                    it.centerVertically(taskView.id, hourView.id)
                }
            }
        }
    }

    private fun setEventWidthConstraints(collections: MutableList<EventCollection>) {
        applyConstraints { set ->
            for (collection in collections) {
                if (collection.events.size > 1) {
                    set.createHorizontalChainRtl(ConstraintSet.PARENT_ID, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.END, collection.events.map { it.id }.toIntArray(),null, ConstraintSet.CHAIN_SPREAD)
                } else if (collection.events.isNotEmpty()) {
                    set.centerHorizontally(collection.events[0].id, ConstraintSet.PARENT_ID)
                }
            }
        }
    }

    inner class Adapter {

        var eventsQuery: Query? = null
            set(value) {
                field = value
                field?.addSnapshotListener { querySnapshot, exception ->
                    events.clear()
                    querySnapshot?.map {
                        val event = it.toObject(Event::class.java)
                        event.ref = it.reference.id
                        return@map event
                    }?.let { events.addAll(it) }
                    events.sortBy { it.startTime }
                    if (exception != null) {
                        throw exception
                    }
                    drawEvents(this.events)
                    drawTasks(this.tasks)
                }
            }
        var tasksQuery: Query? = null
            set(value) {
                field = value
                field?.addSnapshotListener { querySnapshot, exception ->
                    tasks.clear()
                    querySnapshot?.map {
                        val task = it.toObject(Task::class.java)
                        task.ref = it.id
                        return@map task
                    }?.let { tasks.addAll(it) }
                    if (exception != null) {
                        throw exception
                    }
                    drawEvents(this.events)
                    drawTasks(this.tasks)
                }
            }

        private val events = mutableListOf<Event>()
        private val tasks = mutableListOf<Task>()
    }
    data class EventCollection(var startTime: Date, var endTime: Date, val events: MutableList<EventView>)
}
