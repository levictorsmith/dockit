package me.levansmith.dockit.util

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.view.View

class VerticalDragShadowBuilder(view: View): View.DragShadowBuilder(view) {
    private val shadow: View = View(view.context)
    init {
        shadow.layout(0,0, view.width, view.height)
        shadow.background = ColorDrawable(Color.LTGRAY)
    }

    override fun onProvideShadowMetrics(size: Point, touch: Point) {
        size.set(view.width, view.height)
        val width = Resources.getSystem().displayMetrics.widthPixels
        touch.set(width / 2, 0)
    }

    override fun onDrawShadow(canvas: Canvas) {
        shadow.x = 0f
        shadow.draw(canvas)
    }
}