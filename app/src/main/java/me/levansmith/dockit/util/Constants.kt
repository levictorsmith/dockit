package me.levansmith.dockit.util


object Constants {
    object RequestCodes {
        const val SIGN_IN_GOOGLE = 1
    }
    object Actions {
        const val RESTART_NOTIFICATION_SERVICE = "me.levansmith.dockit.util.Actions.RESTART_NOTIFICATION_SERVICE"
    }
    object Types {
        const val EVENT = "me.levansmith.dockit.util.Types.EVENT"
        const val TASK = "me.levansmith.dockit.util.Types.TASK"
    }
    const val TASK_DRAG_DROP_DATA = "me.levansmith.dockit.util.TASK_DRAG_DROP_DATA"
}