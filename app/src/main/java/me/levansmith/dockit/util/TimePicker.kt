package me.levansmith.dockit.util

import android.app.AlertDialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AppCompatActivity
import android.text.format.DateFormat
import org.apache.commons.lang3.time.DateUtils
import java.util.*


class TimePicker : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private lateinit var pickerTag: String
    var listener: OnTimeChangedListener? = null
    private lateinit var calendar: Calendar
    private lateinit var data: Bundle

    override fun onCreateDialog(savedInstanceState: Bundle?) : AlertDialog {
        listener = (activity as? OnTimeChangedListener)
        calendar = DateUtils.truncate(arguments!!.getSerializable(ARG_CALENDAR) as Calendar, Calendar.MINUTE)
        var min = Math.round(calendar.get(Calendar.MINUTE) / 15f) * 15
        val hour = calendar.get(Calendar.HOUR_OF_DAY) + (min / 60)
        min %= 60
        pickerTag = arguments!!.getString(PICKER_TAG)!!
        data = arguments!!.getBundle(ARG_DATA)!!
        return TimePickerDialog(activity, this, hour, min, DateFormat.is24HourFormat(activity))
    }

    override fun onTimeSet(picker: android.widget.TimePicker?, hourOfDay: Int, minute: Int) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)
        calendar = DateUtils.truncate(calendar, Calendar.MINUTE)
        listener?.onTimeChanged(calendar, pickerTag, data)
    }

    interface OnTimeChangedListener {
        fun onTimeChanged(calendar: Calendar, tag: String, data: Bundle = Bundle())
    }

    companion object {

        private const val ARG_CALENDAR = "me.levansmith.dockit.util.Picker.ARG_CALENDAR"
        private const val PICKER_TAG = "me.levansmith.dockit.util.Picker.PICKER_TAG"
        private const val ARG_DATA = "me.levansmith.dockit.util.Picker.ARG_DATA"

        fun newInstance(calendar: Calendar, activity: AppCompatActivity, tag: String, data: Bundle = Bundle()) {
            val args = Bundle()
            args.putSerializable(ARG_CALENDAR, calendar)
            args.putString(PICKER_TAG, tag)
            val dialog = TimePicker()
            dialog.arguments = args
            dialog.arguments?.putBundle(ARG_DATA, data)
            dialog.show(activity.supportFragmentManager, tag)
        }
    }
}