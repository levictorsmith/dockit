package me.levansmith.dockit.util

import android.app.Service
import android.content.Intent
import android.os.IBinder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.levansmith.dockit.DockitApp
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class NotificationService : Service() {

    companion object {
        val TAG = NotificationService::class.java.logTag()
    }

    private var timer: Timer? = null
    private lateinit var timerTask: TimerTask

    private fun scanCache() {
        GlobalScope.launch {
            val now = Date().time
            val reminders = DockitApp.cache.reminderDao().getByTime(now - 10000, now)
            reminders.forEach { reminder ->
                val cal = DateUtils.toCalendar(Date(reminder.datetime))
                val type = reminder.type
                when(type) {
                    Constants.Types.TASK -> {
                        TaskClient.getTask(reminder.itemRef) {
                            NotificationManager.scheduleTaskReminder(this@NotificationService, cal, it)
                        }
                    }
                }
                DockitApp.cache.reminderDao().delete(reminder)
            }
        }
    }

    private fun start() {
        timer = Timer()
        timerTask = kotlin.concurrent.timerTask {
            scanCache()
        }
        timer?.schedule(timerTask, 1000, 1000)
    }

    private fun stop() {
        timer?.cancel()
        timer = null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        LogDog.i(TAG, "Started notification service")
        start()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        LogDog.i(TAG, "Stopped notification service")
        val intent = Intent(Constants.Actions.RESTART_NOTIFICATION_SERVICE)
        sendBroadcast(intent)
        stop()
    }

    override fun onBind(p0: Intent?): IBinder? = null
}