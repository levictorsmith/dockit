package me.levansmith.dockit.util

import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import android.text.format.DateFormat
import me.levansmith.dockit.R
import me.levansmith.dockit.domain.model.Task
import java.util.*


object NotificationManager {

    const val CHANNEL_ID = "me.levansmith.dockit.CHANNEL_ID"

    init {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            // Create the NotificationChannel, but only on API 26+ because
//            // the NotificationChannel class is new and not in the support library
//            val name = DockitApp.self.resources.getString(R.string.channel_name)
//            val description = DockitApp.self.resources.getString(R.string.channel_description)
//            val importance = NotificationManagerCompat.IMPORTANCE_DEFAULT
//            val channel = NotificationChannel(CHANNEL_ID, name, importance)
//            channel.description = description
//            // Register the channel with the system
//            val notificationManager: NotificationManagerCompat = NotificationManagerCompat.from(DockitApp.self)
//            notificationManager.createNotificationChannel(channel)
//        }
    }

    fun scheduleTaskReminder(context: Context, reminderTime: Calendar?, task: Task) {
        if (reminderTime == null) {
            return
        }
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(context)
                .setContentTitle(task.description)
                .setContentText(DateFormat.getTimeFormat(context).format(task.startTime))
                .setSmallIcon(R.drawable.ic_dockit_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher_round))

        val manager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as android.app.NotificationManager
        manager.notify(task.ref.hashCode(), builder.build())
    }
}