package me.levansmith.dockit.util.manager


import android.app.Activity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.levansmith.dockit.R
import me.levansmith.dockit.util.Constants

object AuthManager : OnCompleteListener<AuthResult>, GoogleApiClient.OnConnectionFailedListener {

    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    var user: FirebaseUser? = mAuth.currentUser
    private var listener: AuthListener? = null
    private var mGoogleApiClient: GoogleApiClient? = null

    val uid: String?
        get() = if (user != null) {
            user!!.uid
        } else null

    val isLoggedIn: Boolean
        get() = user != null

    fun subscribe(authListener: AuthListener?): AuthManager {
        listener = authListener
        return this
    }

    fun signout() {
        mAuth.signOut()
    }

    fun createEmailUser(activity: Activity, email: String, password: String) {
        GlobalScope.launch {
            if (user == null) {
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(activity, this@AuthManager)
            }
        }
    }

    fun signInEmailUser(activity: Activity, email: String, password: String) {
        GlobalScope.launch {
            if (user == null) {
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(activity, this@AuthManager)
                        .addOnFailureListener {
                            when(it) {
                                is FirebaseAuthInvalidUserException -> {
                                    createEmailUser(activity, email, password)
                                }
                            }
                        }
            }
        }
    }

    fun signInGoogleUser(activity: androidx.fragment.app.FragmentActivity) {
        if (mGoogleApiClient == null) {
            configureGoogleApiClient(activity)
        }
        val intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        activity.startActivityForResult(intent, Constants.RequestCodes.SIGN_IN_GOOGLE)
    }

    fun authenticateGoogleUser(activity: Activity, account: GoogleSignInAccount?) {
        if (account != null) {
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(activity, this)
        } else {
            listener!!.onError(Exception("Error: Google account passed in is null"))
        }
    }

    private fun configureGoogleApiClient(activity: androidx.fragment.app.FragmentActivity) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(activity)
                .enableAutoManage(activity, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    override fun onComplete(task: Task<AuthResult>) {
        if (task.isSuccessful) {
            user = mAuth.currentUser
            if (listener != null) {
                listener!!.onComplete()
                listener = null
            }
        } else {
            user = null
            if (listener != null) {
                listener!!.onError(Exception(task.exception))
            }
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (listener != null) {
            listener!!.onError(Exception(String.format("Error %d: " + connectionResult.errorMessage!!, connectionResult.errorCode)))
        }
    }

    interface AuthListener {

        fun onComplete()

        fun onError(e: Exception)
    }
}
