package me.levansmith.dockit.util

import android.content.res.Resources
import android.graphics.Rect
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import android.text.Editable
import android.text.TextWatcher
import android.transition.Transition
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.TextView

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.getRect(): Rect {
    return Rect(
            x.toInt(),
            y.toInt(),
            (x + width).toInt(),
            (y + height).toInt()
    )
}

fun ViewGroup.forEachChild(action: ((View) -> Unit)?) {
    (0 until childCount).mapNotNull { getChildAt(it) }.forEach {
        action?.invoke(it)
    }
}

fun ViewGroup.forEachChildIndexed(action: ((Int, View) -> Unit)?) {
    (0 until childCount).mapNotNull { getChildAt(it) }.forEachIndexed { index, view ->
        action?.invoke(index, view)
    }
}

fun ConstraintLayout.applyConstraints(action: (ConstraintSet) -> Unit) {
    val constraintSet = ConstraintSet()
    constraintSet.clone(this)
    action(constraintSet)
    constraintSet.applyTo(this)
}

fun ConstraintSet.toEnd(@IdRes viewId: Int, @IdRes toView: Int) {
    connect(viewId, ConstraintSet.END,toView, ConstraintSet.END)
}

fun ConstraintSet.toEnd(@IdRes views: List<View>, @IdRes toView: Int) {
    views.forEachIndexed { index, view ->
        if (view.id != views.last().id) {
            connect(view.id, ConstraintSet.END, views[index + 1].id, ConstraintSet.START)
        } else {
            connect(view.id, ConstraintSet.END, toView, ConstraintSet.END)
        }
    }
}

fun Animation.setAnimationListener(onStart: () -> Unit = {}, onEnd: () -> Unit = {}) {
    this.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) = Unit

        override fun onAnimationEnd(p0: Animation?) {
            onEnd()
        }

        override fun onAnimationStart(p0: Animation?) {
            onStart()
        }

    })
}

fun TextView.addTextChangedListener(action: ((Editable) -> Unit)) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable) {
            action(p0)
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
    })
}

fun Transition.onTransitionEnd(action: (Transition) -> Unit) {
    this.addListener(object : Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition?) {
            action(transition!!)
        }
        override fun onTransitionStart(p0: Transition?) = Unit
        override fun onTransitionCancel(p0: Transition?) = Unit
        override fun onTransitionPause(p0: Transition?) = Unit
        override fun onTransitionResume(p0: Transition?) = Unit
    })
}

fun Resources.convertDipToPixels(dips: Float): Int {
    return (dips * this.displayMetrics.density + 0.5f).toInt()
}

fun Resources.convertDipToPixels(resource: Int): Int {
    return (getDimension(resource) * displayMetrics.density + 0.5f).toInt()
}
