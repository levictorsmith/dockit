package me.levansmith.dockit.util

import android.graphics.Canvas
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import me.levansmith.logging.logTag

class TouchHelperCallback(private val listener: ItemTouchHelperListener) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        listener.onItemMove(viewHolder, viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        val buffer = viewHolder.itemView.height / 2
        val topY = viewHolder.itemView.top + dY
        val bottomY = topY + viewHolder.itemView.height
        if (topY < -buffer || bottomY > recyclerView.height + buffer && isCurrentlyActive) {
            listener.onItemOutOfBounds(viewHolder, ItemState.OutOfBounds)
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    companion object {
        val TAG = TouchHelperCallback::class.java.logTag()
    }
}

sealed class ItemState {
    object OutOfBounds : ItemState()
    object Moving : ItemState()
    object Idle : ItemState()
}
