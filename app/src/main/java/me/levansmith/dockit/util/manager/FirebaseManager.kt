package me.levansmith.dockit.util.manager

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.DockitApp


object FirebaseManager {
    private const val COLLECTION_EVENT = "event"
    private const val COLLECTION_GOAL = "goal"
    private const val COLLECTION_NOTE = "note"
    private const val COLLECTION_PLANNER = "planner"
    private const val COLLECTION_TASK = "task"


    fun getEventCollection()    :CollectionReference = DockitApp.self.db.collection(COLLECTION_EVENT)
    fun getGoalCollection()     :CollectionReference = DockitApp.self.db.collection(COLLECTION_GOAL)
    fun getNoteCollection()     :CollectionReference = DockitApp.self.db.collection(COLLECTION_NOTE)
    fun getPlannerCollection()  :CollectionReference = DockitApp.self.db.collection(COLLECTION_PLANNER)
    fun getTaskCollection()     :CollectionReference = DockitApp.self.db.collection(COLLECTION_TASK)

    fun getDocument(documentPath: String) : DocumentReference = DockitApp.self.db.document(documentPath)
}