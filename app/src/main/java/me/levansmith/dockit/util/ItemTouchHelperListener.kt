package me.levansmith.dockit.util

import androidx.recyclerview.widget.RecyclerView

interface ItemTouchHelperListener {

    companion object {
        const val UP = 1
        const val DOWN = 0
    }

    fun onItemMove(viewHolder: RecyclerView.ViewHolder, fromPosition: Int, toPosition: Int) {
    }

    fun onItemDismiss(position: Int)

    fun onItemDoubleClick(position: Int) {
    }

    fun onItemOutOfBounds(viewHolder: RecyclerView.ViewHolder, state: ItemState) {}
}
