package me.levansmith.dockit.util

import android.annotation.SuppressLint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import android.widget.TextView

class OnDrawableClickListener(private val position: Position, private val listener: (MotionEvent) -> Boolean) : View.OnTouchListener {

    companion object {
        const val fudgeFactor = 10
    }

    private var drawable: Drawable? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (drawable == null) {
                drawable = (v as TextView).compoundDrawables[position.ordinal]
            }
            val x = event.x
            val y = event.y
            val bounds: Rect = drawable!!.bounds
            if (x >= (v.right - bounds.width() - fudgeFactor) && x <= (v.right - v.paddingRight + fudgeFactor)
                    && y >= (v.paddingTop - fudgeFactor) && y <= (v.height - v.paddingBottom) + fudgeFactor) {
                return listener.invoke(event)
            }
        }
        return false
    }

    enum class Position {
        LEFT, TOP, RIGHT, BOTTOM
    }
}