package me.levansmith.dockit.util

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class DatePicker : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var pickerTag: String
    private lateinit var listener: OnDateChangedListener
    private lateinit var calendar: Calendar

    override fun onCreateDialog(savedInstanceState: Bundle?) : AlertDialog {
        listener = activity as OnDateChangedListener
        calendar = arguments!!.getSerializable(ARG_CALENDAR) as Calendar
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        pickerTag = arguments!!.getString(PICKER_TAG)!!
        return DatePickerDialog(activity!!, this, year, month, day)
    }

    override fun onDateSet(p0: android.widget.DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        calendar.set(year, month, dayOfMonth)
        listener.onDateChanged(calendar, pickerTag)
    }

    interface OnDateChangedListener {
        fun onDateChanged(calendar: Calendar, tag: String)
    }

    companion object {

        const val ARG_CALENDAR = "me.levansmith.dockit.util.Picker.ARG_CALENDAR"
        private const val PICKER_TAG = "me.levansmith.dockit.util.Picker.PICKER_TAG"

        fun newInstance(calendar: Calendar, activity: AppCompatActivity, tag: String = PICKER_TAG) {
            val args = Bundle()
            args.putSerializable(ARG_CALENDAR, calendar)
            args.putString(PICKER_TAG, tag)
            val dialog = DatePicker()
            dialog.arguments = args
            dialog.show(activity.supportFragmentManager, tag)
        }
    }
}