package me.levansmith.dockit.util

import org.apache.commons.lang3.time.DateUtils
import java.util.*

fun hasOverlap(startA: Calendar, endA: Calendar, startB: Calendar, endB: Calendar): Boolean {
    return (startA <= endB && endA >= startB)
}

fun hasOverlap(startA: Date, endA: Date, startB: Date, endB: Date): Boolean {
    return hasOverlap(DateUtils.toCalendar(startA), DateUtils.toCalendar(endA), DateUtils.toCalendar(startB), DateUtils.toCalendar(endB))
}

fun Calendar.isBefore(num: Int, field: Int, otherTime: Calendar): Boolean {
    val before = otherTime.clone() as Calendar
    before.add(field, -num)
    return this in before..otherTime
}
fun Calendar.isBefore(num: Int, field: Int, otherTime: Date): Boolean   = this.isBefore(num, field, DateUtils.toCalendar(otherTime))
fun Date.isBefore(num: Int, field: Int, otherTime: Calendar): Boolean   = DateUtils.toCalendar(this).isBefore(num, field, otherTime)
fun Date.isBefore(num: Int, field: Int, otherTime: Date): Boolean       = this.isBefore(num, field, DateUtils.toCalendar(otherTime))

fun Calendar.isWithin__After(num: Int, field: Int, otherTime: Calendar, backwards: Boolean = false): Boolean {
    val after = otherTime.clone() as Calendar
    after.add(field, num)
    val before = otherTime.clone() as Calendar
    before.add(field, -num)
    return if (backwards) {
        before < this && this > otherTime
    } else {
        after > this && this >= otherTime
    }
}
fun Calendar.isWithin__After(num: Int, field: Int, otherTime: Date, backwards: Boolean): Boolean    = this.isWithin__After(num, field, DateUtils.toCalendar(otherTime), backwards)
fun Date.isWithin__After(num: Int, field: Int, otherTime: Calendar, backwards: Boolean): Boolean    = DateUtils.toCalendar(this).isWithin__After(num, field, otherTime, backwards)
fun Date.isWithin__After(num: Int, field: Int, otherTime: Date, backwards: Boolean): Boolean        = this.isWithin__After(num, field, DateUtils.toCalendar(otherTime), backwards)

/**
 * Exampled: calendar.isWithin(30, Calendar.MINUTE, Calendar.getInstance())
 * If the calendar is represented as:
 * 2018-01-23T12:34:56Z
 * and the comparison calendar is:
 * 2018-01-23T12:00:56Z
 * and the number is 30 and the field is Calendar.MINUTE, then it would return false because the given calendar is neither
 * 30 minutes before or after the comparing time.
 * @return Whether the given calendar is within a given time
 */
fun Calendar.isWithin(num: Int, field: Int, otherTime: Calendar): Boolean = this.isBefore(num, field, otherTime) || this.isWithin__After(num, field, otherTime)
fun Calendar.isWithin(num: Int, field: Int, otherTime: Date): Boolean   = this.isWithin(num, field, DateUtils.toCalendar(otherTime))
fun Date.isWithin(num: Int, field: Int, otherTime: Calendar): Boolean   = DateUtils.toCalendar(this).isWithin(num, field, otherTime)
fun Date.isWithin(num: Int, field: Int, otherTime: Date): Boolean       = DateUtils.toCalendar(this).isWithin(num, field, otherTime)

fun calMorning(): Calendar {
    val cal = DateUtils.truncate(Calendar.getInstance(), Calendar.DAY_OF_MONTH)
    cal.set(Calendar.HOUR_OF_DAY, 8)
    return cal
}
fun calAfternoon(): Calendar {
    val cal = DateUtils.truncate(Calendar.getInstance(), Calendar.DAY_OF_MONTH)
    cal.set(Calendar.HOUR_OF_DAY, 13)
    return cal
}
fun calEvening(): Calendar {
    val cal = DateUtils.truncate(Calendar.getInstance(), Calendar.DAY_OF_MONTH)
    cal.set(Calendar.HOUR_OF_DAY, 18)
    return cal
}

fun dateMorning(): Date = calMorning().time
fun dateAfternoon(): Date = calAfternoon().time
fun dateEvening(): Date = calEvening().time
