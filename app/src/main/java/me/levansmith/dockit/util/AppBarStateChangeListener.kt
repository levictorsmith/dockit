package me.levansmith.dockit.util

import com.google.android.material.appbar.AppBarLayout

/**
 * App bar collapsing state
 * @author Paulo Caldeira <paulo.caldeira></paulo.caldeira>@acin.pt>.
 */
interface AppBarStateChangeListener : AppBarLayout.OnOffsetChangedListener {

    // State
    enum class State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        when {
            i == 0 -> {
                onStateChanged(appBarLayout, State.EXPANDED, Math.abs(i))
            }
            Math.abs(i) >= appBarLayout.totalScrollRange -> {
                onStateChanged(appBarLayout, State.COLLAPSED, Math.abs(i))
            }
            else -> {
                onStateChanged(appBarLayout, State.IDLE, Math.abs(i))
            }
        }
    }

    /**
     * Notifies on state change
     * @param appBarLayout Layout
     * @param state Collapse state
     */
    fun onStateChanged(appBarLayout: AppBarLayout, state: State, i: Int)
}
