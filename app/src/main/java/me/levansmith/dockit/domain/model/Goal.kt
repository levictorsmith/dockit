package me.levansmith.dockit.domain.model


import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.util.*

data class Goal(var ref: String = "",
                var description: String = "",
                var startDate: Date = Date(),
                var type: Int = GoalClient.Type.DAILY.ordinal,
                var plannerId: DocumentReference? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readSerializable() as Date,
            parcel.readInt(),
            FirebaseManager.getDocument(parcel.readString()!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ref)
        parcel.writeString(description)
        parcel.writeSerializable(startDate)
        parcel.writeInt(type)
        parcel.writeString(plannerId?.path)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Goal> {
        override fun createFromParcel(parcel: Parcel): Goal = Goal(parcel)
        override fun newArray(size: Int): Array<Goal?> = arrayOfNulls(size)
    }
}

object GoalClient {

    private val TAG = GoalClient::class.java.logTag()

    fun getAllGoals(plannerId: DocumentReference, callback: (List<Goal>) -> Unit) {
        FirebaseManager.getGoalCollection().whereEqualTo(Fields.PLANNER_ID, plannerId).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObjects(Goal::class.java)) }
    }

    fun getDayGoalsQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val nextDay = DateUtils.addDays(truncatedDate, 1)
        return FirebaseManager.getGoalCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereEqualTo(Fields.TYPE, Type.DAILY.ordinal)
                .whereGreaterThanOrEqualTo(Fields.START_DATE, truncatedDate)
                .whereLessThan(Fields.START_DATE, nextDay)
    }

    fun getWeekGoalsQuery(plannerId: DocumentReference, date: Date): Query {
        val calendar = DateUtils.toCalendar(DateUtils.truncate(date, Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.DAY_OF_WEEK, calendar.firstDayOfWeek)
        val firstDayOfWeek = calendar.time
        calendar.add(Calendar.WEEK_OF_YEAR, 1)
        val firstDayOfNextWeek = calendar.time
        return FirebaseManager.getGoalCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereEqualTo(Fields.TYPE, Type.WEEKLY.ordinal)
                .whereGreaterThanOrEqualTo(Fields.START_DATE, firstDayOfWeek)
                .whereLessThan(Fields.START_DATE, firstDayOfNextWeek)
    }

    fun getLongTermGoalsQuery(plannerId: DocumentReference, date: Date): Query {
        return FirebaseManager.getGoalCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereEqualTo(Fields.TYPE, Type.LONG_TERM.ordinal)
                .whereLessThanOrEqualTo(Fields.START_DATE, date)
    }

    fun getGoal(key: String, callback: (Goal) -> Unit) {
        FirebaseManager.getGoalCollection().document(key).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObject(Goal::class.java)!!) }
    }

    fun addGoal(goal: Goal) {
        FirebaseManager.getGoalCollection().add(goal)
                .addOnFailureListener { exception -> LogDog.w(TAG, "AddGoal", exception) }
    }

    fun updateGoal(key: String, updates: Map<String, Any>) {
        FirebaseManager.getGoalCollection().document(key).update(updates)
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateGoal", exception) }
    }

    fun updateGoal(goal: Goal) {
        FirebaseManager.getGoalCollection()
                .document(goal.ref)
                .update(mutableMapOf<String, Any>().apply {
                    put(Fields.DESCRIPTION, goal.description)
                    put(Fields.START_DATE, goal.startDate)
                    put(Fields.TYPE, goal.type)
                    put(Fields.PLANNER_ID, goal.plannerId!!)
                })
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateGoal", exception) }
    }

    fun deleteGoal(key: String) {
        FirebaseManager.getGoalCollection().document(key).delete()
                .addOnFailureListener { exception -> LogDog.w(TAG, "DeleteGoal", exception) }
    }

    object Fields {
        const val DESCRIPTION = "description"
        const val START_DATE = "startDate"
        const val TYPE = "type"
        const val PLANNER_ID = "plannerId"
    }

    enum class Type {
        DAILY, WEEKLY, LONG_TERM
    }
}
