package me.levansmith.dockit.domain

import androidx.annotation.StringDef
import me.levansmith.dockit.util.Constants

@StringDef(Constants.Types.EVENT, Constants.Types.TASK)
annotation class ReminderType
