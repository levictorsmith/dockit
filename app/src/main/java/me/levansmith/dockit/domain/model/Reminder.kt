package me.levansmith.dockit.domain.model

import org.apache.commons.lang3.time.DateUtils
import java.util.*

data class Reminder(var num: Int, var unit: String) {

    companion object {

        const val ARG_TIME = "time"
        const val ARG_TYPE = "type"
        const val ARG_ITEM_REF = "itemRef"
        const val ARG_TIME_NUM = "num"
        const val ARG_TIME_UNIT = "unit"

        const val MINUTES = "MINUTES"
        const val HOURS = "HOURS"
        const val DAYS = "DAYS"
        const val WEEKS = "WEEKS"

        fun getReminderTime(date: Date, time: HashMap<String, Any>): Date {
            return when (time[ARG_TIME_UNIT] as String) {
                MINUTES -> DateUtils.addMinutes(date, -(time[ARG_TIME_NUM] as Int))
                HOURS -> DateUtils.addHours(date, -(time[ARG_TIME_NUM] as Int))
                DAYS -> DateUtils.addDays(date, -(time[ARG_TIME_NUM] as Int))
                WEEKS -> DateUtils.addWeeks(date, -(time[ARG_TIME_NUM] as Int))
                else -> DateUtils.addMinutes(date, -(time[ARG_TIME_NUM] as Int)) //Default to minutes
            }
        }

        fun getReminderText(time: HashMap<String, Any>): String {
            return "${time[ARG_TIME_NUM]} ${(time[ARG_TIME_UNIT] as String).toLowerCase().capitalize()}"
        }
    }
}
