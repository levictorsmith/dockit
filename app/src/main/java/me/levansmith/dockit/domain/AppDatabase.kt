package me.levansmith.dockit.domain

import androidx.room.Database
import androidx.room.RoomDatabase
import me.levansmith.dockit.domain.dao.ReminderDao
import me.levansmith.dockit.domain.entity.ReminderEntity

@Database(entities = [ReminderEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun reminderDao(): ReminderDao
}