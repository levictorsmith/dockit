package me.levansmith.dockit.domain.model


import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.util.*

data class Event(var ref: String = "",
                 var title: String = "",
                 var description: String = "",
                 var startTime: Date = Date(),
                 var endTime: Date = Date(),
                 var plannerId: DocumentReference? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readSerializable() as Date,
            parcel.readSerializable() as Date,
            FirebaseManager.getDocument(parcel.readString()!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ref)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeSerializable(startTime)
        parcel.writeSerializable(endTime)
        parcel.writeString(plannerId?.path)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Event> {
        override fun createFromParcel(parcel: Parcel): Event = Event(parcel)
        override fun newArray(size: Int): Array<Event?> = arrayOfNulls(size)
    }
}

object EventClient {

    private val TAG = EventClient::class.java.logTag()

    fun getDayEventsQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val nextDay = DateUtils.addDays(truncatedDate, 1)
        return FirebaseManager.getEventCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.START_TIME, truncatedDate)
                .whereLessThan(Fields.START_TIME, nextDay)
                .orderBy(Fields.START_TIME)
    }

    fun getDayEvents(plannerId: DocumentReference, date: Date, callback: (List<Event>) -> Unit) {
        getDayEventsQuery(plannerId, date)
                .addSnapshotListener { snapshot, _ ->
                    if (snapshot != null) {
                        callback(snapshot.toObjects(Event::class.java))
                    }
                }
    }

    fun getEvent(key: String, callback: (Event) -> Unit) {
        FirebaseManager.getEventCollection().document(key).get().addOnSuccessListener { snapshot -> callback(snapshot.toObject(Event::class.java)!!) }
    }

    fun addEvent(event: Event) {
        FirebaseManager.getEventCollection().add(event).addOnFailureListener { exception -> LogDog.w(TAG, "AddEvent", exception) }
    }

    fun updateEvent(key: String, updates: Map<String, Any>) {
        FirebaseManager.getEventCollection().document(key).update(updates).addOnFailureListener { exception -> LogDog.w(TAG, "UpdateEvent", exception) }
    }

    fun updateEvent(event: Event) {
        FirebaseManager.getEventCollection()
                .document(event.ref)
                .update(mutableMapOf<String, Any>().apply {
                    put(Fields.TITLE, event.title)
                    put(Fields.DESCRIPTION, event.description)
                    put(Fields.START_TIME, event.startTime)
                    put(Fields.END_TIME, event.endTime)
                    put(Fields.PLANNER_ID, event.plannerId!!)
                })
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateEvent", exception) }
    }

    fun deleteEvent(key: String) {
        FirebaseManager.getEventCollection().document(key).delete().addOnFailureListener { exception -> LogDog.w(TAG, "DeleteEvent", exception) }
    }

    object Fields {
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val START_TIME = "startTime"
        const val END_TIME = "endTime"
        const val SUBMISSION_TIME = "submissionTime"
        const val PLANNER_ID = "plannerId"
    }
}
