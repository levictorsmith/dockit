package me.levansmith.dockit.domain.model


import com.firebase.ui.firestore.SnapshotParser
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.gson.*
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.lang.reflect.Type
import java.util.*

data class Task(var ref: String = "",
                var description: String = "",
                var label: String = "",
                var startTime: Date = Date(),
                var completed: Boolean = false,
                var position: Long = 1,
                //TODO: Remove the terrible map-within-a-list pattern
                var reminders: MutableList<MutableMap<String, Any>> = mutableListOf(),
                var plannerId: DocumentReference? = null) {
    companion object {
        fun serialize(task: Task): String {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Task::class.java, TaskSerializer())
                    .create()
            return gson.toJson(task)!!
        }
        fun deserialize(json: String): Task {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Task::class.java, TaskSerializer())
                    .create()
            return gson.fromJson(json, Task::class.java)
        }
    }
}

class TaskSerializer : JsonSerializer<Task>, JsonDeserializer<Task>, SnapshotParser<Task> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Task {
        //TODO: Figure out Reminders and planner...
        val obj = json.asJsonObject
        return Task(
                obj[TaskClient.Fields.REF].asString,
                obj[TaskClient.Fields.DESCRIPTION].asString,
                obj[TaskClient.Fields.LABEL].asString,
                Date(obj[TaskClient.Fields.START_TIME].asLong),
                obj[TaskClient.Fields.COMPLETED].asBoolean,
                obj[TaskClient.Fields.POSITION].asLong,
                mutableListOf(),
                FirebaseManager.getDocument(obj[TaskClient.Fields.PLANNER_ID].asString)
        )
    }

    override fun serialize(task: Task, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        return JsonObject().apply {
            addProperty(TaskClient.Fields.REF, task.ref)
            addProperty(TaskClient.Fields.DESCRIPTION, task.description)
            addProperty(TaskClient.Fields.LABEL, task.label)
            addProperty(TaskClient.Fields.START_TIME, task.startTime.time)
            addProperty(TaskClient.Fields.COMPLETED, task.completed)
            addProperty(TaskClient.Fields.POSITION, task.position)
            // TODO: Figure out reminders
            addProperty(TaskClient.Fields.PLANNER_ID, task.plannerId?.path)
        }
    }

    override fun parseSnapshot(snapshot: DocumentSnapshot): Task {
        return Task(
                snapshot.id,
                snapshot[TaskClient.Fields.DESCRIPTION] as String,
                snapshot[TaskClient.Fields.LABEL] as String,
                (snapshot[TaskClient.Fields.START_TIME] as Timestamp).toDate(),
                snapshot[TaskClient.Fields.COMPLETED] as Boolean,
                snapshot[TaskClient.Fields.POSITION] as Long,
                if(snapshot[TaskClient.Fields.REMINDERS] != null) (snapshot[TaskClient.Fields.REMINDERS] as MutableList<MutableMap<String, Any>>) else mutableListOf(),
                snapshot[TaskClient.Fields.PLANNER_ID] as DocumentReference
        )
    }
}

object TaskClient {

    private val TAG = TaskClient::class.java.logTag()

    fun getDayTasksQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val nextDay = DateUtils.addDays(truncatedDate, 1)
        return FirebaseManager.getTaskCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.START_TIME, truncatedDate)
                .whereLessThan(Fields.START_TIME, nextDay)
    }

    fun getMorningTasksQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val sixAm = DateUtils.setHours(truncatedDate, 6)
        val noon = DateUtils.setHours(truncatedDate, 12)
        return FirebaseManager.getTaskCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.START_TIME, sixAm)
                .whereLessThan(Fields.START_TIME, noon)
                .orderBy(Fields.START_TIME)
    }

    fun getAfternoonTasksQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val noon = DateUtils.setHours(truncatedDate, 12)
        val fivePM = DateUtils.setHours(truncatedDate, 17)
        return FirebaseManager.getTaskCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.START_TIME, noon)
                .whereLessThan(Fields.START_TIME, fivePM)
                .orderBy(Fields.START_TIME)
    }

    fun getEveningTasksQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val fivePM = DateUtils.setHours(truncatedDate, 17)
        val elevenPM = DateUtils.setHours(truncatedDate, 23)
        return FirebaseManager.getTaskCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.START_TIME, fivePM)
                .whereLessThan(Fields.START_TIME, elevenPM)
                .orderBy(Fields.START_TIME)
    }

    fun getUndoneTasksQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        return FirebaseManager.getTaskCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereLessThan(Fields.START_TIME, truncatedDate)
                .whereEqualTo(Fields.COMPLETED, false)
                .orderBy(Fields.START_TIME, Query.Direction.ASCENDING)
    }

    fun getTask(key: String, callback: (Task) -> Unit) {
        FirebaseManager.getTaskCollection().document(key).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObject(Task::class.java)!!) }
    }

    fun addTask(task: Task, onSuccess: (DocumentReference) -> Unit = {}) {
        task.ref = ""
        FirebaseManager.getTaskCollection().add(task)
                .addOnFailureListener { exception -> LogDog.w(TAG, "AddTask", exception) }
                .addOnSuccessListener { onSuccess(it) }
    }

    fun updateTask(key: String, updates: Map<String, Any>) {
        FirebaseManager.getTaskCollection().document(key).update(updates)
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateTask", exception) }
    }

    fun updateTask(task: Task) {
        FirebaseManager.getTaskCollection()
                .document(task.ref)
                .update(mutableMapOf<String, Any>().apply {
                    put(Fields.DESCRIPTION, task.description)
                    put(Fields.LABEL, task.label)
                    put(Fields.START_TIME, task.startTime)
                    put(Fields.COMPLETED, task.completed)
                    put(Fields.POSITION, task.position)
                    put(Fields.REMINDERS, task.reminders)
                    put(Fields.PLANNER_ID, task.plannerId!!)
                })
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateTask", exception) }
    }

    fun deleteTask(key: String) {
        FirebaseManager.getTaskCollection().document(key).delete()
                .addOnFailureListener { exception -> LogDog.w(TAG, "DeleteTask", exception) }
    }

    object Fields {
        const val REF = "ref"
        const val DESCRIPTION = "description"
        const val LABEL = "label"
        const val START_TIME = "startTime"
        const val COMPLETED = "completed"
        const val POSITION = "position"
        const val REMINDERS = "reminders"
        const val PLANNER_ID = "plannerId"
    }

}
