package me.levansmith.dockit.domain.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import me.levansmith.dockit.domain.entity.ReminderEntity

@Dao
interface ReminderDao {
    @Query("SELECT * FROM reminder")
    fun getAll(): List<ReminderEntity>

    @Query("SELECT * FROM reminder WHERE datetime BETWEEN :startTime AND :endTime")
    fun getByTime(startTime: Long, endTime: Long): List<ReminderEntity>

    @Insert
    fun insert(reminderEntity: ReminderEntity)

    @Delete
    fun delete(reminderEntity: ReminderEntity)
}