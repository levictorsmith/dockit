package me.levansmith.dockit.domain.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import me.levansmith.dockit.domain.ReminderType

@Entity(tableName = "reminder")
data class ReminderEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        @ColumnInfo(name = "datetime")
        var datetime: Long = 0,
        @ColumnInfo(name = "type")
        @ReminderType
        var type: String = "",
        @ColumnInfo(name = "item_ref")
        var itemRef: String = ""
)
