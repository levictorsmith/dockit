package me.levansmith.dockit.domain.model


import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import org.apache.commons.lang3.time.DateUtils
import java.util.*

data class Note(var ref: String = "",
                var content: String = "",
                var date: Date = Date(),
                var archived: Boolean = false,
                var pinned: Boolean = false,
                var prioritized: Boolean = false,
                var plannerId: DocumentReference? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readSerializable() as Date,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            FirebaseManager.getDocument(parcel.readString()!!))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ref)
        parcel.writeString(content)
        parcel.writeSerializable(date)
        parcel.writeByte(if (archived) 1 else 0)
        parcel.writeByte(if (pinned) 1 else 0)
        parcel.writeByte(if (prioritized) 1 else 0)
        parcel.writeString(plannerId?.path)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Note> {
        override fun createFromParcel(parcel: Parcel): Note = Note(parcel)
        override fun newArray(size: Int): Array<Note?> = arrayOfNulls(size)
    }
}

object NoteClient {

    private val TAG = NoteClient::class.java.logTag()

    fun getDayNotesQuery(plannerId: DocumentReference, date: Date): Query {
        val truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
        val nextDay = DateUtils.addDays(truncatedDate, 1)
        return FirebaseManager.getNoteCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereGreaterThanOrEqualTo(Fields.DATE, truncatedDate)
                .whereLessThan(Fields.DATE, nextDay)
                .whereEqualTo(Fields.PINNED, false)
    }

    fun getDayNotes(plannerId: DocumentReference, date: Date, callback: (List<Note>) -> Unit) {
        getDayNotesQuery(plannerId, date).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObjects(Note::class.java)) }
                .addOnFailureListener { exception -> LogDog.w(TAG, "Error: ", exception) }
    }

    fun getPinnedNotesQuery(plannerId: DocumentReference): Query {
        return FirebaseManager.getNoteCollection()
                .whereEqualTo(Fields.PLANNER_ID, plannerId)
                .whereEqualTo(Fields.PINNED, true)
    }

    fun getPinnedNotes(plannerId: DocumentReference, callback: (List<Note>) -> Unit) {
        getPinnedNotesQuery(plannerId).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObjects(Note::class.java)) }
                .addOnFailureListener { exception -> LogDog.w(TAG, "Error: ", exception) }
    }

    fun getNote(key: String, callback: (Note) -> Unit) {
        FirebaseManager.getNoteCollection().document(key).get()
                .addOnSuccessListener { snapshot -> callback(snapshot.toObject(Note::class.java)!!) }
    }

    fun addNote(note: Note) {
        FirebaseManager.getNoteCollection().add(note)
                .addOnFailureListener { exception -> LogDog.w(TAG, "AddNote", exception) }
    }

    fun updateNote(key: String, updates: Map<String, Any>) {
        FirebaseManager.getNoteCollection().document(key).update(updates)
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateNote", exception) }
    }

    fun updateNote(note: Note) {
        FirebaseManager.getNoteCollection()
                .document(note.ref)
                .update(mutableMapOf<String, Any>().apply {
                    put(Fields.CONTENT, note.content)
                    put(Fields.DATE, note.date)
                    put(Fields.ARCHIVED, note.archived)
                    put(Fields.PINNED, note.pinned)
                    put(Fields.PRIORITIZED, note.prioritized)
                    put(Fields.PLANNER_ID, note.plannerId!!)
                })
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdateNote", exception) }
    }

    fun deleteNote(key: String) {
        FirebaseManager.getNoteCollection().document(key).delete()
                .addOnFailureListener { exception -> LogDog.w(TAG, "DeleteNote", exception) }
    }

    object Fields {
        const val CONTENT = "content"
        const val DATE = "date"
        const val ARCHIVED = "archived"
        const val PINNED = "pinned"
        const val PRIORITIZED = "prioritized"
        const val PLANNER_ID = "plannerId"
    }

}
