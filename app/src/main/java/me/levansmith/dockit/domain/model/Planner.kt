package me.levansmith.dockit.domain.model

import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.util.manager.AuthManager
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logdog.android.LogDog
import me.levansmith.logging.logTag
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


data class Planner(var title: String = "",
                   val userId: String? = AuthManager.uid)

object PlannerClient {

    private val TAG = PlannerClient::class.java.logTag()

    fun getMainPlanner(callback: (DocumentReference?) -> Unit) {
        FirebaseManager.getPlannerCollection().whereEqualTo(Fields.USER_ID, AuthManager.uid).limit(1).get()
                .addOnSuccessListener { snapshot ->
                    if (snapshot.documentChanges.isEmpty()) {
                        callback(null)
                    } else {
                        callback(snapshot.documents[0].reference)
                    }
                }
                .addOnFailureListener { callback(null) }
    }

    suspend fun getPlanner(key: String): DocumentReference {
        return suspendCoroutine {
            FirebaseManager.getPlannerCollection().document(key).get()
                    .addOnSuccessListener { snapshot -> it.resume(snapshot.reference) }
        }
    }

    fun addPlanner(planner: Planner) {
        FirebaseManager.getPlannerCollection().add(planner)
                .addOnFailureListener { exception -> LogDog.w(TAG, "AddPlanner", exception) }
    }

    fun addPlanner(planner: Planner, onComplete: () -> Unit) {
        FirebaseManager.getPlannerCollection().add(planner)
                .addOnSuccessListener { onComplete() }
                .addOnFailureListener { exception -> LogDog.w(TAG, "AddPlanner", exception) }
    }

    fun updatePlanner(key: String, updates: Map<String, Any>) {
        FirebaseManager.getPlannerCollection().document(key).update(updates)
                .addOnFailureListener { exception -> LogDog.w(TAG, "UpdatePlanner", exception) }
    }

    fun deletePlanner(key: String) {
        FirebaseManager.getPlannerCollection().document(key).delete()
                .addOnFailureListener { exception -> LogDog.w(TAG, "DeletePlanner", exception) }
    }

    object Fields {
        const val TITLE = "title"
        const val USER_ID = "userId"
    }
}
