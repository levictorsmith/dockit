package me.levansmith.dockit.dashboard

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_note.*
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ActivityNoteBinding
import me.levansmith.dockit.domain.model.Note
import me.levansmith.dockit.domain.model.NoteClient
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.util.*
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class NoteActivity : AppCompatActivity(), View.OnClickListener, DatePicker.OnDateChangedListener, AppBarStateChangeListener {

    companion object {

        const val EDIT = "me.levansmith.dockit.dashboard.NoteActivity.EDIT"

        fun startAdd(context: Context?) {
            val intent = Intent(context, NoteActivity::class.java)
            context?.startActivity(intent)
        }

        fun startEdit(context: Context, note: Note) {
            val intent = Intent(context, NoteActivity::class.java)
            intent.putExtra(EDIT, note)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityNoteBinding
    private var isEdit: Boolean = false
    private var showContent: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
    }

    private fun initUI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_note)
        isEdit = intent.hasExtra(EDIT)
        setSupportActionBar(toolbar)
        binding.note = if (isEdit) intent.getParcelableExtra(EDIT) else Note()
        PlannerClient.getMainPlanner { planner ->
            binding.note!!.plannerId = planner
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(if (isEdit) R.string.title_activity_edit_note else R.string.title_activity_add_note)
        app_bar.addOnOffsetChangedListener(this)
        fab.setImageResource(if (isEdit) R.drawable.ic_done_white_24dp else R.drawable.ic_add_white_24dp)
        fab.setOnClickListener(this)
        binding.actionAddSave.setText(if (isEdit) R.string.action_save_changes else R.string.title_activity_add_note)
        binding.notePinned.setOnClickListener { view ->
            val button = view as ImageButton
            button.setImageResource(if (binding.note!!.pinned) R.drawable.ic_pin_off_black_24dp else R.drawable.ic_pin_black_24dp)
            binding.note!!.pinned = !binding.note!!.pinned
        }
        binding.notePrioritized.setOnClickListener { view ->
            val button = view as ImageButton
            button.setImageResource(if (binding.note!!.prioritized) R.drawable.ic_error_outline_black_24dp else R.drawable.ic_error_red_24dp)
            binding.note!!.prioritized = !binding.note!!.prioritized
        }
        note_content_top.addTextChangedListener {
            if (note_content_top.isFocused) {
                note_content_bottom.text = it
            }
        }
        note_content_bottom.addTextChangedListener {
            if (note_content_bottom.isFocused) {
                note_content_top.text = it
            }
        }
    }

    fun addNote() {
        NoteClient.addNote(binding.note!!)
        finish()
    }

    fun editNote() {
        NoteClient.updateNote(binding.note!!)
        finish()
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.note_date -> {
                val cal = DateUtils.toCalendar(binding.note?.date)
                DatePicker.newInstance(cal, this)
            }
            R.id.action_add_save -> {
                if (isEdit) {
                    editNote()
                } else {
                    addNote()
                }
            }
            R.id.fab -> {
                if (isEdit) {
                    editNote()
                } else {
                    addNote()
                }
            }
        }
    }

    override fun onDateChanged(calendar: Calendar, tag: String) {
        binding.note!!.date = calendar.time
        binding.invalidateAll()
    }

    override fun onStateChanged(appBarLayout: AppBarLayout, state: AppBarStateChangeListener.State, i: Int) {
        if (i >= 160 && showContent) {
            note_content_container_top.hide()
            note_content_container_bottom.show()
            showContent = !showContent
        } else if (i < 150 && !showContent) {
            note_content_container_top.show()
            note_content_container_bottom.hide()
            showContent = !showContent
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
