package me.levansmith.dockit.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import me.levansmith.dockit.util.manager.AuthManager

abstract class DrawerActivity : AppCompatActivity() {
    fun onSignout(item: MenuItem) {
        AuthManager.signout()
    }
}