package me.levansmith.dockit.dashboard

import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ListItemNoteBinding
import me.levansmith.dockit.domain.model.Note
import me.levansmith.dockit.domain.model.NoteClient
import me.levansmith.dockit.util.ItemTouchHelperListener
import me.levansmith.dockit.util.hide
import me.levansmith.dockit.util.show


class NotesAdapter(options: FirestoreRecyclerOptions<Note>, private val rootView: View) : FirestoreRecyclerAdapter<Note, NotesAdapter.NoteHolder>(options), ItemTouchHelperListener {

    private var tempDeletedItem: Note? = null

    override fun onItemDismiss(position: Int) {
        tempDeletedItem = getItem(position)
        NoteClient.deleteNote(snapshots.getSnapshot(position).id)
        Snackbar.make(rootView, "Deleted task", Snackbar.LENGTH_LONG)
                .setAction("Undo") {
                    if (tempDeletedItem != null) {
                        NoteClient.addNote(tempDeletedItem!!)
                    }
                }
                .setActionTextColor(ContextCompat.getColor(rootView.context, R.color.colorAccent))
                .show()
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int, model: Note) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        return NoteHolder(ListItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    inner class NoteHolder(private val binding: ListItemNoteBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            NoteActivity.startEdit(binding.root.context, binding.note!!)
        }

        fun bind(note: Note) {
            binding.note = note
            binding.note!!.ref = snapshots.getSnapshot(adapterPosition).id
            if (!binding.note!!.prioritized && !binding.note!!.pinned) {
                binding.icons.hide()
            } else {
                binding.icons.show()
            }
        }
    }
}

