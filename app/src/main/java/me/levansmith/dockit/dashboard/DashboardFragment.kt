package me.levansmith.dockit.dashboard

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.FragmentDashboardBinding
import me.levansmith.dockit.domain.model.Goal
import me.levansmith.dockit.domain.model.GoalClient
import me.levansmith.dockit.domain.model.Note
import me.levansmith.dockit.domain.model.NoteClient
import me.levansmith.dockit.goals.GoalActivity
import me.levansmith.dockit.goals.GoalsAdapter
import me.levansmith.dockit.util.OnDrawableClickListener
import me.levansmith.dockit.util.TouchHelperCallback
import me.levansmith.dockit.util.manager.FirebaseManager
import me.levansmith.logging.logTag
import java.util.*


class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding
    private lateinit var planner: DocumentReference
    private lateinit var genericNotesAdapter: NotesAdapter
    private lateinit var pinnedNotesAdapter: NotesAdapter
    private lateinit var dailyGoalsAdapter: GoalsAdapter
    private lateinit var weeklyGoalsAdapter: GoalsAdapter
    private lateinit var longTermGoalsAdapter: GoalsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        planner = FirebaseManager.getDocument(arguments!!.getString(ARG_PLANNER)!!)

        //TODO: Change the date here to the selected date
        val genericOptions: FirestoreRecyclerOptions<Note> = FirestoreRecyclerOptions.Builder<Note>()
                .setQuery(NoteClient.getDayNotesQuery(planner, Date()), Note::class.java)
                .build()
        genericNotesAdapter = NotesAdapter(genericOptions, binding.root)
        binding.genericNotes.adapter = genericNotesAdapter

        val pinnedOptions: FirestoreRecyclerOptions<Note> = FirestoreRecyclerOptions.Builder<Note>()
                .setQuery(NoteClient.getPinnedNotesQuery(planner), Note::class.java)
                .build()
        pinnedNotesAdapter = NotesAdapter(pinnedOptions, binding.root)
        binding.pinnedNotes.adapter = pinnedNotesAdapter

        val dailyOptions = FirestoreRecyclerOptions.Builder<Goal>()
                .setQuery(GoalClient.getDayGoalsQuery(planner, Date()), Goal::class.java)
                .build()
        val weeklyOptions = FirestoreRecyclerOptions.Builder<Goal>()
                .setQuery(GoalClient.getWeekGoalsQuery(planner, Date()), Goal::class.java)
                .build()
        val longTermOptions = FirestoreRecyclerOptions.Builder<Goal>()
                .setQuery(GoalClient.getLongTermGoalsQuery(planner, Date()), Goal::class.java)
                .build()

        dailyGoalsAdapter = GoalsAdapter(dailyOptions)
        weeklyGoalsAdapter = GoalsAdapter(weeklyOptions)
        longTermGoalsAdapter = GoalsAdapter(longTermOptions)

        binding.dailyGoals.adapter = dailyGoalsAdapter
        binding.weeklyGoals.adapter = weeklyGoalsAdapter
        binding.longTermGoals.adapter = longTermGoalsAdapter
        binding.goalsHeader.setOnTouchListener(OnDrawableClickListener(OnDrawableClickListener.Position.RIGHT) {
            GoalActivity.startAdd(context)
            return@OnDrawableClickListener true
        })
        binding.notesHeader.setOnTouchListener(OnDrawableClickListener(OnDrawableClickListener.Position.RIGHT) {
            NoteActivity.startAdd(context)
            return@OnDrawableClickListener true
        })
        ItemTouchHelper(TouchHelperCallback(genericNotesAdapter)).attachToRecyclerView(binding.genericNotes)
        ItemTouchHelper(TouchHelperCallback(pinnedNotesAdapter)).attachToRecyclerView(binding.pinnedNotes)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        genericNotesAdapter.startListening()
        pinnedNotesAdapter.startListening()
        dailyGoalsAdapter.startListening()
        weeklyGoalsAdapter.startListening()
        longTermGoalsAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        genericNotesAdapter.stopListening()
        pinnedNotesAdapter.stopListening()
        dailyGoalsAdapter.stopListening()
        weeklyGoalsAdapter.stopListening()
        longTermGoalsAdapter.stopListening()
    }

    companion object {

        var LOG_TAG = DashboardFragment::class.java.logTag()
        const val ARG_PLANNER = "me.levansmith.dockit.dashboard.DashboardFragment.ARG_PLANNER"

        fun newInstance(plannerPath: String) : DashboardFragment {
            val fragment = DashboardFragment()
            val args = Bundle()
            args.putString(ARG_PLANNER, plannerPath)
            fragment.arguments = args
            return fragment
        }
    }
}