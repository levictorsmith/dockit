package me.levansmith.dockit.dashboard

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.appcompat.widget.Toolbar
import android.view.View
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.R
import me.levansmith.dockit.component.CustomAHBottomNavigationFABBehavior
import me.levansmith.dockit.databinding.ActivityDashboardBinding
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.dockit.goals.GoalActivity
import me.levansmith.dockit.schedule.EventActivity
import me.levansmith.dockit.schedule.ScheduleFragment
import me.levansmith.dockit.tasks.TaskActivity
import me.levansmith.dockit.tasks.TasksAdapter
import me.levansmith.dockit.tasks.TasksFragment
import me.levansmith.dockit.util.NotificationService
import me.levansmith.dockit.util.TimePicker
import java.util.*

class DashboardActivity : DrawerActivity(), AHBottomNavigation.OnTabSelectedListener, View.OnClickListener, TimePicker.OnTimeChangedListener {

    private lateinit var binding: ActivityDashboardBinding
    private lateinit var planner: DocumentReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)

        PlannerClient.getMainPlanner { reference -> planner = reference!!; initUI() }
        //TODO: Set up nav drawer in toolbar
        startService(Intent(this, NotificationService::class.java))
    }

    private fun initUI() {
        findViewById<Toolbar>(R.id.action_bar).getChildAt(0).setOnClickListener {
            binding.bottomNavigation.currentItem = 1
        }
        val navAdapter = AHBottomNavigationAdapter(this, R.menu.navigation)
        navAdapter.setupWithBottomNavigation(binding.bottomNavigation, resources.getIntArray(R.array.bottom_nav_colors))
        binding.bottomNavigation.setOnTabSelectedListener(this)
        if (binding.floatingActionMenu.parent is CoordinatorLayout) {
            (binding.floatingActionMenu.layoutParams as CoordinatorLayout.LayoutParams).behavior = CustomAHBottomNavigationFABBehavior()
        }
        binding.bottomNavigation.isColored = true
        binding.floatingActionMenu.rootFAB.setOnClickListener(this)
        binding.viewPager.offscreenPageLimit = 4

        val fragments = listOf(
                TasksFragment.newInstance(planner.path),
                DashboardFragment.newInstance(planner.path),
                ScheduleFragment.newInstance(planner.path)
        )
        binding.viewPager.adapter = DashboardViewPagerAdapter(supportFragmentManager, fragments)
        binding.bottomNavigation.currentItem = 1
        binding.handler = this
    }

    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        when (position) {
            0 -> {
                if (binding.floatingActionMenu.isOpen) {
                    binding.floatingActionMenu.close()
                }
            }
            1 -> {
            }
            2 -> {
                if (binding.floatingActionMenu.isOpen) {
                    binding.floatingActionMenu.close()
                }
            }
            else -> return false
        }
        binding.viewPager.setCurrentItem(position, false)
        return true
    }

    override fun onTimeChanged(calendar: Calendar, tag: String, data: Bundle) {
        if (tag == TasksAdapter.TIME_PICKER) {
            val key = data.getString(TaskClient.Fields.REF)!!
            TaskClient.updateTask(key, mutableMapOf<String, Any>().apply {
                put(TaskClient.Fields.START_TIME, calendar.time)
            })
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.fam_root_fab -> {
                when (binding.viewPager.currentItem) {
                    TASKS_TAB -> {
                        TaskActivity.startAdd(this)
                    }
                    DASHBOARD_TAB -> {
                        binding.floatingActionMenu.toggle()
                    }
                    SCHEDULE_TAB -> {
                        EventActivity.startAdd(this)
                    }
                }
            }
            R.id.add_event -> {
                EventActivity.startAdd(this)
            }
            R.id.add_task -> {
                TaskActivity.startAdd(this)
            }
            R.id.add_goal -> {
                GoalActivity.startAdd(this)
            }
            R.id.add_note -> {
                NoteActivity.startAdd(this)
            }
        }
    }

    companion object {

        private val LOG_TAG = DashboardActivity::class.java.toString()
        const val TASKS_TAB = 0
        const val DASHBOARD_TAB = 1
        const val SCHEDULE_TAB = 2

        fun start(context: Context) {
            val intent = Intent(context, DashboardActivity::class.java)
            context.startActivity(intent)
        }
    }
}
