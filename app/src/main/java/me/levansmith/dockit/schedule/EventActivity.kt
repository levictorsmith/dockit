package me.levansmith.dockit.schedule

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_event.*
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.ActivityEventBinding
import me.levansmith.dockit.domain.model.Event
import me.levansmith.dockit.domain.model.EventClient
import me.levansmith.dockit.domain.model.PlannerClient
import me.levansmith.dockit.util.*
import org.apache.commons.lang3.time.DateUtils
import java.util.*

class EventActivity : AppCompatActivity(), TimePicker.OnTimeChangedListener, DatePicker.OnDateChangedListener, View.OnClickListener, AppBarStateChangeListener {

    companion object {

        const val START_TIME = "me.levansmith.dockit.schedule.EventActivity.START_TIME"
        const val END_TIME = "me.levansmith.dockit.schedule.EventActivity.END_TIME"
        const val START_DATE = "me.levansmith.dockit.schedule.EventActivity.START_DATE"
        const val END_DATE = "me.levansmith.dockit.schedule.EventActivity.END_DATE"
        const val EDIT = "me.levansmith.dockit.schedule.EventActivity.EDIT"

        private const val ARG_CALENDAR = "me.levansmith.dockit.schedule.EventActivity.ARG_CALENDAR"

        fun startAdd(context: Context, calendar: Calendar? = null) {
            val intent = Intent(context, EventActivity::class.java)
            if (calendar != null) {
                intent.putExtra(ARG_CALENDAR, calendar)
            }
            context.startActivity(intent)
        }

        fun startEdit(context: Context, event: Event) {
            val intent = Intent(context, EventActivity::class.java)
            intent.putExtra(EDIT, event)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityEventBinding
    private var isEdit: Boolean = false
    private var showTitle: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_event)
        isEdit = intent.hasExtra(EDIT)
        toolbar.setNavigationOnClickListener {
            when(it.id) {
                R.id.home -> {
                    onBackPressed()
                }
            }
        }
        setSupportActionBar(toolbar)
        binding.event = if (isEdit) intent.getParcelableExtra(EDIT) else Event()
        if (intent.hasExtra(ARG_CALENDAR)) {
            val calendar = DateUtils.truncate(intent.getSerializableExtra(ARG_CALENDAR) as Calendar, Calendar.MINUTE)
            binding.event!!.startTime = calendar.time
            val extraHour = DateUtils.toCalendar(calendar.time)
            extraHour.add(Calendar.HOUR_OF_DAY, 1)
            binding.event!!.endTime = extraHour.time
        } else if (!isEdit) {
            val newEnd = DateUtils.truncate(DateUtils.toCalendar(binding.event!!.endTime), Calendar.MINUTE)
            newEnd.add(Calendar.HOUR_OF_DAY, 1)
            binding.event!!.endTime = newEnd.time
        }
        PlannerClient.getMainPlanner { planner ->
            binding.event!!.plannerId = planner
        }
        supportActionBar?.setTitle(if (isEdit) R.string.title_activity_edit_event else R.string.title_activity_add_event)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        app_bar.addOnOffsetChangedListener(this)
        fab.setImageResource(if (isEdit) R.drawable.ic_done_white_24dp else R.drawable.ic_add_white_24dp)
        fab.setOnClickListener(this)
        binding.actionAddSave.setText(if (isEdit) R.string.action_save_changes else R.string.title_activity_add_event)

        event_title_top.addTextChangedListener {
            if (event_title_top.isFocused) {
                event_title_bottom.text = it
            }
        }
        event_title_bottom.addTextChangedListener {
            if (event_title_bottom.isFocused) {
                event_title_top.text = it
            }
        }
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.event_start_date -> {
                val cal = DateUtils.toCalendar(binding.event?.startTime)
                DatePicker.newInstance(cal, this, START_DATE)
            }
            R.id.event_start_time -> {
                val cal = DateUtils.toCalendar(binding.event?.startTime)
                TimePicker.newInstance(cal, this, START_TIME)
            }
            R.id.event_end_date -> {
                val cal = DateUtils.toCalendar(binding.event?.endTime)
                DatePicker.newInstance(cal, this, END_DATE)
            }
            R.id.event_end_time -> {
                val cal = DateUtils.toCalendar(binding.event?.endTime)
                TimePicker.newInstance(cal, this, END_TIME)
            }
            R.id.action_add_save -> {
                if (isEdit) {
                    editEvent()
                } else {
                    addEvent()
                }
            }
            R.id.fab -> {
                if (isEdit) {
                    editEvent()
                } else {
                    addEvent()
                }
            }
        }
    }

    fun addEvent() {
        if (binding.event!!.endTime > binding.event!!.startTime) {
            EventClient.addEvent(binding.event!!)
            finish()
        }
    }

    fun editEvent() {
        if (binding.event!!.endTime > binding.event!!.startTime) {
            EventClient.updateEvent(binding.event!!)
            finish()
        }
    }

    override fun onDateChanged(calendar: Calendar, tag: String) {
        when (tag) {
            START_DATE -> {
                binding.event!!.startTime = calendar.time
                if (binding.event!!.endTime.before(binding.event!!.startTime)) {
                    val newEnd = DateUtils.toCalendar(binding.event!!.endTime)
                    newEnd.set(Calendar.YEAR, calendar[Calendar.YEAR])
                    newEnd.set(Calendar.MONTH, calendar[Calendar.MONTH])
                    newEnd.set(Calendar.DAY_OF_MONTH, calendar[Calendar.DAY_OF_MONTH])
                    binding.event!!.endTime = newEnd.time
                }
                binding.invalidateAll()
            }
            END_DATE -> {
                binding.event!!.endTime = calendar.time
                binding.invalidateAll()
            }
        }
    }

    override fun onTimeChanged(calendar: Calendar, tag: String, data: Bundle) {
        when (tag) {
            START_TIME -> {
                binding.event!!.startTime = calendar.time
                if (binding.event!!.endTime.before(binding.event!!.startTime)) {
                    val newEnd = DateUtils.toCalendar(binding.event!!.endTime)
                    newEnd.set(Calendar.HOUR_OF_DAY, calendar[Calendar.HOUR_OF_DAY] + 1)
                    newEnd.set(Calendar.MINUTE, calendar[Calendar.MINUTE])
                    binding.event!!.endTime = newEnd.time
                }
                binding.invalidateAll()
            }
            END_TIME -> {
                binding.event!!.endTime = calendar.time
                binding.invalidateAll()
            }
        }
    }

    override fun onStateChanged(appBarLayout: AppBarLayout, state: AppBarStateChangeListener.State, i: Int) {
        if (i >= 160 && showTitle) {
            event_title_container_top.hide()
            event_title_container_bottom.show()
            showTitle = !showTitle
        } else if (i < 150 && !showTitle) {
            event_title_container_top.show()
            event_title_container_bottom.hide()
            showTitle = !showTitle
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
