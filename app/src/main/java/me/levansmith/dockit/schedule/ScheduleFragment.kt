package me.levansmith.dockit.schedule

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.DocumentReference
import me.levansmith.dockit.R
import me.levansmith.dockit.databinding.FragmentScheduleBinding
import me.levansmith.dockit.domain.model.EventClient
import me.levansmith.dockit.domain.model.TaskClient
import me.levansmith.dockit.util.manager.FirebaseManager
import java.util.*


class ScheduleFragment : Fragment() {

    companion object {

        const val ARG_PLANNER = "me.levansmith.dockit.schedule.ScheduleFragment.ARG_PLANNER"

        fun newInstance(plannerPath: String): ScheduleFragment {
            val fragment = ScheduleFragment()
            val args = Bundle()
            args.putString(ARG_PLANNER, plannerPath)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var binding: FragmentScheduleBinding
    private lateinit var planner: DocumentReference
    //TODO: Change the date here to the selected date
    private var calendar: Calendar = Calendar.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedule, container, false)
        planner = FirebaseManager.getDocument(arguments!!.getString(ARG_PLANNER)!!)
        binding.schedule.date = calendar

        binding.schedule.adapter.eventsQuery = EventClient.getDayEventsQuery(planner, calendar.time)
        binding.schedule.adapter.tasksQuery = TaskClient.getDayTasksQuery(planner, calendar.time)
        return binding.root
    }
}